
#include "cmsis_os.h"                                           // CMSIS RTOS header file
#include "CLOCK.h"

/*----------------------------------------------------------------------------
 *      Timer: Sample timer functions
 *---------------------------------------------------------------------------*/
 

/*----- One-Shoot Timer Example -----*/
unsigned int uiShowCounter=0;
static void Timer_ShowTime (void const *arg);                  // prototype for timer callback function
static osTimerId id1;                                           // timer id
static uint32_t  exec1;                                         // argument for the timer call back function
static osTimerDef (Timer1, Timer_ShowTime);                    // define timers

// One-Shoot Timer Function
static void Timer_ShowTime (void const *arg) {
  // add user code here
//	if(uiShowCounter==0) {
		printf("\r\n"); CLOCK_ShowNow();
//	} else {
//		printf("\r"); CLOCK_ShowNow();
//	}
	uiShowCounter++;
}

// Example: Create and Start timers
void Init_Timers (void) {
  osStatus status;                                              // function return status
 
  // Create one-shoot timer
  exec1 = 1;
  // id1 = osTimerCreate (osTimer(Timer1), osTimerOnce, &exec1);
  id1 = osTimerCreate (osTimer(Timer1), osTimerPeriodic, &exec1);
  if (id1 != NULL) {    // One-shot timer created
    // start timer with delay 1000ms
    status = osTimerStart (id1, 1000);            
    if (status != osOK) {
      // Timer could not be started
    }
  }
 

}
