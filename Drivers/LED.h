#ifndef __LED_H
#define __LED_H

#include "stm32f4xx.h"

typedef struct _LED_PIN {
  GPIO_TypeDef *port;
  uint16_t       num;
} LED_PIN;

extern struct _LED_PIN LED[];
extern unsigned short usLedNumber;

#define LED_On(px) (LED[px].port->BSRRL = LED[px].num)
#define LED_Off(px) (LED[px].port->BSRRH = LED[px].num)

void LED_Setup(void);

#endif
