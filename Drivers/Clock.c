#include "Clock.h"
#include <string.h>
#include <math.h>

RTC_TimeTypeDef RTC_SetTimeStructure;
RTC_DateTypeDef RTC_SetDateStructure;	
RTC_TimeTypeDef RTC_NowTimeStructure;
RTC_DateTypeDef RTC_NowDateStructure;
__IO uint32_t AsynchPrediv = 0, SynchPrediv = 0;

const char DefaultDateString[]={FIRMWARE_DATE};
const char DefaultTimeString[]={FIRMWARE_TIME};
char DateString[24];
char *MonthString[12]={"Jun", "Fed", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
char *MonthFullString[]={"January", "February", "March ", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
char DateTimeString[36];
char *WeekDayString[7]={"Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"};
char *WeekDayFullString[]={"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
char TimeString[12];

char ClockString[32];

void CLOCK_Init(void)
{
	/* Enable the PWR clock */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

	/* Allow access to RTC */
	PWR_BackupAccessCmd(ENABLE);

#if defined (RTC_CLOCK_SOURCE_LSI)  /* LSI used as RTC source clock*/
/* The RTC Clock may varies due to LSI frequency dispersion. */
	/* Enable the LSI OSC */ 
	RCC_LSICmd(ENABLE);

	/* Wait till LSI is ready */  
	while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
	{
	}

	/* Select the RTC Clock Source */
	RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);

	SynchPrediv = 249;
	AsynchPrediv = 127;

#elif defined (RTC_CLOCK_SOURCE_LSE) /* LSE used as RTC source clock */
	/* Enable the LSE OSC */
	RCC_LSEConfig(RCC_LSE_ON);

	/* Wait till LSE is ready */  
	while(RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
	{
	}

	/* Select the RTC Clock Source */
	RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);

	SynchPrediv = 255;
	AsynchPrediv = 127;
	
	// Clock = RTC_Clock / ( (AsynchPrediv+1) * (SynchPrediv+1) )
    
#else
	#error Please select the RTC Clock source inside the main.c file
#endif /* RTC_CLOCK_SOURCE_LSI */

	/* Enable the RTC Clock */
	RCC_RTCCLKCmd(ENABLE);

	/* Wait for RTC APB registers synchronisation */
	RTC_WaitForSynchro();

	/* Enable The TimeStamp */
	RTC_TimeStampCmd(RTC_TimeStampEdge_Falling, ENABLE);    
}

void CLOCK_makeDateString(void)
{
	/* Get the current Date */
	RTC_GetDate(RTC_Format_BIN, &RTC_NowDateStructure);
	sprintf(DateString, "%0.4d/%0.2d/%0.2d %s", RTC_NowDateStructure.RTC_Year+2000, RTC_NowDateStructure.RTC_Month, RTC_NowDateStructure.RTC_Date, WeekDayString[RTC_NowDateStructure.RTC_WeekDay%7]);
}


void CLOCK_makeTimeString(void)
{
	/* Get the current Time */
	RTC_GetTime(RTC_Format_BIN, &RTC_NowTimeStructure);
	sprintf(TimeString, "%0.2d:%0.2d:%0.2d", RTC_NowTimeStructure.RTC_Hours, RTC_NowTimeStructure.RTC_Minutes, RTC_NowTimeStructure.RTC_Seconds);
}

void CLOCK_makeDateTimeString(void)
{
	CLOCK_makeDateString();
	CLOCK_makeTimeString();
	sprintf(DateTimeString, "%s %s", DateString, TimeString);
}

uint8_t CLOCK_GetWeekDay(RTC_DateTypeDef *RTC_DateStructure)
{
	double calc_m[]={11,12,1,2,3,4,5,6,7,8,9,10};
	double calc_d=(double)RTC_DateStructure->RTC_Date;
	double calc_a=20, calc_b, calc_x;
	uint8_t calc_WeekDay;
	
	if( (RTC_DateStructure->RTC_Month==1) || (RTC_DateStructure->RTC_Month==2) ) {
		calc_b=(double)(RTC_DateStructure->RTC_Year-1);
	} else {
		calc_b=(double)RTC_DateStructure->RTC_Year;	
	}
	calc_x=calc_d+(2.6*calc_m[RTC_DateStructure->RTC_Month-1]-0.2)-2.0*calc_a+calc_b+(calc_a/4.0)+(calc_b/4.0);
	calc_x=floor(calc_x);
	calc_WeekDay=((u32)calc_x%7);
	RTC_DateStructure->RTC_WeekDay=calc_WeekDay;
	return calc_WeekDay;
}

void CLOCK_SetDefault(void)
{
	unsigned short i;
	unsigned int u32Year, u32Month, u32Day;
	unsigned int u32Hour, u32Minute, u32Second;
	
	// const char DefaultDateString[]={__DATE__};
  sprintf(ClockString, "%c%c%c", DefaultDateString[0], DefaultDateString[1], DefaultDateString[2]);
	// Get Month
	for(i=0;i<12;i++) if(!strncmp(ClockString, MonthString[i], strlen(ClockString))) break;
	u32Month=i+1;
	
	// Get Day & Year
	sscanf(&DefaultDateString[4],"%d %d", &u32Day, &u32Year);
	u32Year-=2000;

	// Get Hour, Minute & Second
	sscanf(DefaultTimeString,"%d:%d:%d", &u32Hour, &u32Minute, &u32Second);	
	
	/* Configure the time register */
	RTC_SetDateStructure.RTC_Year = u32Year;
	RTC_SetDateStructure.RTC_Month = u32Month;
	RTC_SetDateStructure.RTC_Date = u32Day;
	CLOCK_GetWeekDay(&RTC_SetDateStructure);

	/* Configure the RTC date register */
	if(RTC_SetDate(RTC_Format_BIN, &RTC_SetDateStructure) == ERROR) {
		ShowOSTime(); printf("CLOCK: => RTC Set Date failed. !! <<");
	} else {
		ShowOSTime(); printf("CLOCK: => RTC Set Date success: ");
		CLOCK_ShowDate();
		/* Indicator for the RTC configuration */
		RTC_WriteBackupRegister(RTC_BKP_DR0, DEF_BKP_DR0);
	}

	RTC_SetTimeStructure.RTC_Hours=u32Hour;
	RTC_SetTimeStructure.RTC_Minutes=u32Minute;
	RTC_SetTimeStructure.RTC_Seconds=u32Second;
		
	/* Configure the RTC time register */
	if(RTC_SetTime(RTC_Format_BIN, &RTC_SetTimeStructure) == ERROR) {
		ShowOSTime(); printf("CLOCK: => RTC Set Time failed.");
	} else {
		ShowOSTime(); printf("CLOCK: => RTC Set Time success: ");
		CLOCK_ShowTime();
		/* Indicator for the RTC configuration */
		RTC_WriteBackupRegister(RTC_BKP_DR0, DEF_BKP_DR0);
	}	
}

unsigned int CLOCK_GetDefault(void)
{
	unsigned int u32Hour, u32Minute, u32Second;

	// Get Hour, Minute & Second
	sscanf(DefaultTimeString,"%d:%d:%d", &u32Hour, &u32Minute, &u32Second);

	return ( u32Hour<<11 | 
					 u32Minute<<5 | 
					 u32Second/2 );	
}

void CLOCK_Calibration(unsigned char *ucString)
{
	unsigned int u32WeekDay;
	unsigned int u32Year, u32Month, u32Day;
	unsigned int u32Hour, u32Minute, u32Second;

  sscanf((char *)ucString,"%d,%d,%d,%d,%d,%d,%d", &u32Year, &u32Month, &u32Day, &u32WeekDay, &u32Hour, &u32Minute, &u32Second);

	RTC_SetDateStructure.RTC_Year = u32Year;
	RTC_SetDateStructure.RTC_Month = u32Month;
	RTC_SetDateStructure.RTC_Date = u32Day;
	CLOCK_GetWeekDay(&RTC_SetDateStructure);

	/* Configure the time register */
	RTC_SetDateStructure.RTC_Year = u32Year;
	RTC_SetDateStructure.RTC_Month = u32Month;
	RTC_SetDateStructure.RTC_Date = u32Day;
	CLOCK_GetWeekDay(&RTC_SetDateStructure);

	/* Configure the RTC date register */
	if(RTC_SetDate(RTC_Format_BIN, &RTC_SetDateStructure) == ERROR) {
		ShowOSTime(); printf(" => RTC Set Date failed. !! <<");
	} else {
		ShowOSTime(); printf(" => RTC Set Date success: ");
		CLOCK_ShowDate();
		/* Indicator for the RTC configuration */
		RTC_WriteBackupRegister(RTC_BKP_DR0, DEF_BKP_DR0);
	}

	RTC_SetTimeStructure.RTC_Hours=u32Hour;
	RTC_SetTimeStructure.RTC_Minutes=u32Minute;
	RTC_SetTimeStructure.RTC_Seconds=u32Second;
		
	/* Configure the RTC time register */
	if(RTC_SetTime(RTC_Format_BIN, &RTC_SetTimeStructure) == ERROR) {
		ShowOSTime(); printf(" => RTC Set Time failed.");
	} else {
		ShowOSTime(); printf(" => RTC Set Time success: ");
		CLOCK_ShowTime();
		/* Indicator for the RTC configuration */
		RTC_WriteBackupRegister(RTC_BKP_DR0, DEF_BKP_DR0);
	}	
}

void CLOCK_Setup(void) 
{
	RTC_InitTypeDef   	RTC_InitStructure;
// 	EXTI_InitTypeDef 	EXTI_InitStructure;
	
	if (RTC_ReadBackupRegister(RTC_BKP_DR0) != DEF_BKP_DR0)
	{
		/* RTC configuration  */
    ShowOSTime(); printf("CLOCK: RTC not yet configured ...");
		CLOCK_Init();
    ShowOSTime(); printf("CLOCK: RTC Configuring ...");

		/* Configure the RTC data register and RTC prescaler */
		RTC_InitStructure.RTC_AsynchPrediv = AsynchPrediv;
		RTC_InitStructure.RTC_SynchPrediv = SynchPrediv;
		RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;
   
		/* Check on RTC init */
		if (RTC_Init(&RTC_InitStructure) == ERROR) {
			ShowOSTime(); printf("CLOCK: => RTC Prescaler Config failed");
		} else {
			ShowOSTime(); printf("CLOCK: => Success.");			
		}
		CLOCK_SetDefault();

	} else {
		/* Check if the Power On Reset flag is set */
		if (RCC_GetFlagStatus(RCC_FLAG_PORRST) != RESET) {	
			ShowOSTime(); printf("CLOCK: Power On Reset occurred....");
		/* Check if the Pin Reset flag is set */
		} else if (RCC_GetFlagStatus(RCC_FLAG_PINRST) != RESET) {
			ShowOSTime(); printf("CLOCK: External Reset occurred....");
		}

		ShowOSTime(); printf("CLOCK: => No need to configure RTC....");
    
		/* Enable the PWR clock */
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

		/* Allow access to RTC */
		PWR_BackupAccessCmd(ENABLE);

		/* Wait for RTC APB registers synchronisation */
		RTC_WaitForSynchro();

		/* Clear the RTC Alarm Flag */
		RTC_ClearFlag(RTC_FLAG_ALRAF);
		
		/* Clear the EXTI Line 17 Pending bit (Connected internally to RTC Alarm) */
		EXTI_ClearITPendingBit(EXTI_Line17);

	}
	CLOCK_makeDateTimeString();
	ShowOSTime(); printf("CLOCK: Now %s", DateTimeString);
	
  //RTC_ClearFlag(RTC_FLAG_WUTF);
  //RTC_ClearITPendingBit(RTC_IT_WUT);
    
	
	//EXTI_ClearITPendingBit(EXTI_Line22);
  //EXTI_ClearITPendingBit(EXTI_Line22);
  //EXTI_InitStructure.EXTI_Line = EXTI_Line22;
  //EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
  //EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;   //?????
  //EXTI_InitStructure.EXTI_LineCmd = ENABLE;
  //EXTI_Init(&EXTI_InitStructure);   
   

	// Enable the RTC Wakeup Interrupt 
  //RTC_ITConfig(RTC_IT_WUT, ENABLE);

  //RTC_WakeUpClockConfig(RTC_WakeUpClock_CK_SPRE_16bits);
  //RTC_SetWakeUpCounter(0);                                           //?�RTC_WakeUpClockConfig�???????
	
  // Enable Wakeup Counter 
  //RTC_WakeUpCmd(ENABLE);        

                                                                                                                                                                                                                                        //?�RTC_WakeUpClock_CK_SPRE_16bits� ?,                                                                                                                                                                                                                       //RTC_SetWakeUpCounter??????0,???1???,??                                                                                                                                                                                                                                  //???2????????,????      	
	//RTC_WakeUpClockConfig(RTC_WakeUpClock_CK_SPRE_16bits);
	//RTC_SetWakeUpCounter(0x0);		
	//RTC_ITConfig(RTC_IT_WUT, ENABLE);

    //RTC_ClearITPendingBit(RTC_IT_WUT);
    //EXTI_ClearITPendingBit(EXTI_Line22);

	/* Enable Wakeup Counter */
	//RTC_WakeUpCmd(ENABLE);
}





