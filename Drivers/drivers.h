#ifndef __DRIVERS_H
#define __DRIVERS_H

#include <stdio.h>
#include "stm32f4xx.h"
#include "LED.h"
#include "clock.h"
#include "ff.h"
#include "sdcard.h"
#include "diskio.h"
#include "sensor.h"
#include "gprs.h"
 
extern unsigned short usDeviceValue;
extern unsigned short usDeviceIDNumber;
extern const unsigned short DeviceID[];
extern const char *DeviceIDString[];
extern const unsigned short usDeviceRAMSize[];
extern unsigned short usDeviceRevValue;
extern unsigned short usDeviceRevIDNumber;
extern const unsigned short DeviceRevID[];
extern const char *DeviceRevIDString[];

#define ID_UNIQUE_ADDRESS 0x1FFF7A10
#define CPU_GetID(x)   ((x >= 0 && x < 3) ? (*(uint32_t *) (ID_UNIQUE_ADDRESS + 4 * (x))) : 0)
#define ID_FLASH_ADDRESS 0x1FFF7A22
#define CPU_GetFlashSize() (*(uint16_t *) (ID_FLASH_ADDRESS))
#define ID_DBGMCU_IDCODE 0xE0042000
#define CPU_GetSignature() ((*(uint16_t *) (ID_DBGMCU_IDCODE)) & 0x0FFF)
#define CPU_GetRevision() (*(uint16_t *) (DBGMCU->IDCODE + 2))

//
// Definition for UART Debug Port
//
#define DEBUG_UART USART1
#define DEBUG_UART_AF GPIO_AF_USART1
#define DEBUG_UART_Baudrate 460800
#define DEBUG_UART_RCC_APBnPeriphClockCmd RCC_APB2PeriphClockCmd
#define DEBUG_UART_RCC_APBnPeriph_USART RCC_APB2Periph_USART1
#define DEBUG_UART_TX_RCC_APBnPeriphClockCmd RCC_APB1PeriphClockCmd
#define DEBUG_UART_TX_RCC_APBnPeriph_GPIO RCC_AHB1Periph_GPIOA
#define DEBUG_UART_TX_Port GPIOA
#define DEBUG_UART_TX_Pin GPIO_Pin_9
#define DEBUG_UART_TX_SRC GPIO_PinSource9
#define DEBUG_UART_RX_RCC_APBnPeriphClockCmd RCC_APB1PeriphClockCmd
#define DEBUG_UART_RX_RCC_APBnPeriph_GPIO RCC_AHB1Periph_GPIOA
#define DEBUG_UART_RX_Port GPIOA
#define DEBUG_UART_RX_Pin GPIO_Pin_10
#define DEBUG_UART_RX_SRC GPIO_PinSource10

extern uint32_t os_time;
extern RCC_ClocksTypeDef SystemClocks;
extern uint16_t ADC_ConvValues[];

void RCC_Setup(void);
void NVIC_Setup(void);
void ADC_DMA_Setup(void);
void SD_FatFS_ShowFSError(unsigned char ucFSErrorCode);
FRESULT SD_FatFS_Setup(void);
void DEBUG_UART_Setup(u32 u32baudrate);

#endif
