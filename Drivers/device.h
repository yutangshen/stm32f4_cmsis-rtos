#ifndef __DEVICE_H
#define __DEVICE_H

#include "stm32f4xx.h"

/**
 * With this "function" you are able to get signature of device.
 *
 * Possible returns:
 * 	- 0x0413: STM32F405xx/07xx and STM32F415xx/17xx)
 *	- 0x0419: STM32F42xxx and STM32F43xxx
 *	- 0x0423: STM32F401xB/C
 *	- 0x0433: STM32F401xD/E
 *	- 0x0431: STM32F411xC/E
 *
 * Returned data is in 16-bit mode, but only bits 11:0 are valid, bits 15:12 are always 0.
 * Defined as macro
 */
#define TM_ID_GetSignature()	((*(uint16_t *) (DBGMCU )) & 0x0FFF)

/**
 * With this "function" you are able to get revision of device.
 *
 * Revisions possible:
 *	- 0x1000: Revision A
 *	- 0x1001: Revision Z
 *	- 0x1003: Revision Y
 *	- 0x1007: Revision 1
 *	- 0x2001: Revision 3
 *
 * Returned data is in 16-bit mode.
 * Defined as macro
 */
#define TM_ID_GetRevision()		(*(uint16_t *) (ID_DBGMCU_IDCODE + 2))

/**
 * With this "function" you are able to get flash size of device.
 *
 * Returned data is in 16-bit mode, returned value is flash size in kB (kilo bytes).
 * Defined as macro
 */
#define TM_ID_GetFlashSize()	(*(uint16_t *) (ID_FLASH_ADDRESS))

/**
 * With this "function" you are able to get unique ID of device in 8-bit (byte) read mode.
 * Unique ID is 96bit long, but if you need just some parts of it, you can read them with 8bit function.
 *
 * Parameters:
 * 	- uint8_t x:
 * 		Value between 0 and 11, corresponding to byte you want to read from 96bits (12bytes)
 *
 * Returned data is 8-bit
 * Defined as macro
 */
#define TM_ID_GetUnique8(x)		((x >= 0 && x < 12) ? (*(uint8_t *) (ID_UNIQUE_ADDRESS + (x))) : 0)
	
/**
 * With this "function" you are able to get unique ID of device in 16-bit (byte) read mode.
 * Unique ID is 96bit long, but if you need just some parts of it, you can read them with 16bit function.
 *
 * Parameters:
 * 	- uint8_t x:
 * 		Value between 0 and 5, corresponding to 2-bytes you want to read from 96bits (12bytes)
 *
 * Returned data is 16-bit
 * Defined as macro
 */
#define TM_ID_GetUnique16(x)	((x >= 0 && x < 6) ? (*(uint16_t *) (ID_UNIQUE_ADDRESS + 2 * (x))) : 0)

/**
 * With this "function" you are able to get unique ID of device in 32-bit (byte) read mode.
 * Unique ID is 96bit long, but if you need just some parts of it, you can read them with 32bit function.
 *
 * Parameters:
 * 	- uint8_t x:
 * 		Value between 0 and 2, corresponding to 4-bytes you want to read from 96bits (12bytes)
 *
 * Returned data is 32-bit
 * Defined as macro
 */
#define TM_ID_GetUnique32(x)	((x >= 0 && x < 3) ? (*(uint32_t *) (ID_UNIQUE_ADDRESS + 4 * (x))) : 0)

#endif
