#include "LED.h"
#include "config.h"

unsigned short usLedNumber;

struct _LED_PIN LED[]={   
{ GPIOH, GPIO_Pin_2}, 
{ GPIOH, GPIO_Pin_3}, 
{ GPIOI, GPIO_Pin_8}, 
{ GPIOI, GPIO_Pin_10} };

void LED_InitGPIO(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	unsigned short i;
	
	usLedNumber=sizeof(LED)/sizeof(struct _LED_PIN);
	
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;

	for(i=0;i<usLedNumber;i++) {
		GPIO_InitStructure.GPIO_Pin = LED[i].num;
		GPIO_Init(LED[i].port, &GPIO_InitStructure); // ��l�� PA10	
	}	
}

void LED_Setup(void)
{
	LED_InitGPIO();
	ShowOSTime(); printf("LED: GPIO Initial was Finished");
}
