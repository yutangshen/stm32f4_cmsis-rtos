#ifndef __CLOCK_H
#define __CLOCK_H

#include <stdio.h>

#include "stm32f4xx.h"
#include "config.h"

#define DEF_BKP_DR0 CLOCK_GetDefault()

#define RTC_CLOCK_SOURCE_LSE


struct CLOCK_PROFILE {
  unsigned short usYear;
  unsigned short usMonth;
  unsigned short usDay;
  unsigned short usHour;
  unsigned short usMinute;
  unsigned short usSecond;  
};

extern RTC_TimeTypeDef RTC_NowTimeStructure;
extern RTC_DateTypeDef RTC_NowDateStructure;
extern char DateString[];
extern char TimeString[];
extern char DateTimeString[];

#define CLOCK_ShowDate() { CLOCK_makeDateString(); printf("%s ", DateString); }
#define CLOCK_ShowTime() { CLOCK_makeTimeString(); printf("%s ", TimeString); }
#define CLOCK_ShowNow() { CLOCK_makeDateTimeString(); printf("%s ", DateTimeString); }


void CLOCK_Init(void);
void CLOCK_Calibration(unsigned char *ucString);
void CLOCK_makeDateString(void);
void CLOCK_makeTimeString(void);
void CLOCK_makeDateTimeString(void);
void CLOCK_Setup(void);
unsigned int CLOCK_GetDefault(void);

#endif
