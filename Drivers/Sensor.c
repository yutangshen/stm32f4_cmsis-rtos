#include "Sensor.h"

#define IntReferenceVoltage ((float)1.21)
#define ExtReferenceVoltage ((float)2.048)

float GetSystemVoltage(void)
{
	return IntReferenceVoltage*((float)4095.0/SensorAverage.fInternalReferenceVoltage);
}
