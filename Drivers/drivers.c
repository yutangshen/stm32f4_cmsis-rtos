#include "drivers.h"

RCC_ClocksTypeDef 			SystemClocks;

static USART_InitTypeDef USART_InitStructure;
static GPIO_InitTypeDef GPIO_InitStructure;
static NVIC_InitTypeDef NVIC_InitStructure;

unsigned short usDeviceIDNumber;
unsigned short usDeviceValue;
const unsigned short DeviceID[] = {0x0413, 0x0419, 0x0423, 0x0433, 0x0431}; 
const char *DeviceIDString[]={
"STM32F405xx/07xx and STM32F415xx/17xx",
"STM32F42xxx and STM32F43xxx",
"STM32F401xB/C",
"STM32F401xD/E",
"STM32F411xC/E",
"Undefined"};
const unsigned short usDeviceRAMSize[]={192, 256, 96, 96, 128};

unsigned short usDeviceRevIDNumber;
unsigned short usDeviceRevValue;
const unsigned short DeviceRevID[] = {0x1000, 0x1001, 0x1003, 0x1007, 0x2001};
const char *DeviceRevIDString[]={
"Revision A",
"Revision Z",
"Revision Y",
"Revision 1",
"Revision 3",
"Undefined"};	
	 
void ShowOSTime(void)
{
	u32 u32time = os_time;;	
	printf("\r\n[%d.%0.3d] ", u32time/1000, u32time%1000);
}

void RCC_Setup(void)
{
	SystemCoreClockUpdate();
	RCC_GetClocksFreq(&SystemClocks);	
	
	RCC_AHB1PeriphClockCmd( RCC_AHB1Periph_GPIOA | RCC_AHB1Periph_GPIOB | RCC_AHB1Periph_GPIOC | RCC_AHB1Periph_GPIOD |
						    RCC_AHB1Periph_GPIOE | RCC_AHB1Periph_GPIOH | RCC_AHB1Periph_GPIOI, ENABLE);
	
	// ------------------------------------------------------------------------- 
	//  SYSCFG Clock Enabled ...
	// ------------------------------------------------------------------------- 	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
	
	usDeviceIDNumber=sizeof(DeviceID)/sizeof(unsigned short);
	usDeviceRevIDNumber=sizeof(DeviceRevID)/sizeof(unsigned short);
}

/*******************************************************************************
* Function Name  : SetupNVIC
* Description    : Configures NVIC
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void NVIC_Setup(void)
{

	/* Configure the NVIC Preemption Priority Bits */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
	// ShowOSTime(); printf("NVIC: the NVIC Preemption Priority Bits was Configured");

	/* Enable the SysTick Interrupt */
	//NVIC_EnableIRQ(SysTick_IRQn);
	
	/* Enable the SDIO IRQ Channel */
	NVIC_InitStructure.NVIC_IRQChannel = SDIO_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* Enable the DMA2_Stream3 Channel */
	NVIC_InitStructure.NVIC_IRQChannel = SD_SDIO_DMA_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);	

	/* Enable the USART1 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

/*******************************************************************************
* Function Name  : FATFS_Init
* Description    : FAT File System Initialization
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
static FATFS *pFS;
static FATFS fs, fstest;                        // Work area (file system object) for logical drive
const char *strFSType[]={
"FAT12",
"FAT16",
"FAT32"};
const char *strFSErrorCode[]={
"FR_OK, Succeeded",
"FR_DISK_ERR, A hard error occurred in the low level disk I/O layer",
"FR_INT_ERR, Assertion failed",
"FR_NOT_READY, The physical drive cannot work",
"FR_NO_FILE, Could not find the file",
"FR_NO_PATH, Could not find the path",
"FR_INVALID_NAME, The path name format is invalid",
"FR_DENIED, Access denied due to prohibited access or directory full",
"FR_EXIST,	Access denied due to prohibited access",
"FR_INVALID_OBJECT, The file/directory object is invalid",
"FR_WRITE_PROTECTED, The physical drive is write protected",
"FR_INVALID_DRIVE, The logical drive number is invalid",
"FR_NOT_ENABLED, The volume has no work area",
"FR_NO_FILESYSTEM, There is no valid FAT volume",
"FR_MKFS_ABORTED, The f_mkfs() aborted due to any parameter error",
"FR_TIMEOUT, Could not get a grant to access the volume within defined period",
"FR_LOCKED, The operation is rejected according to the file sharing policy",
"FR_NOT_ENOUGH_CORE,	LFN working buffer could not be allocated",
"FR_TOO_MANY_OPEN_FILES,	Number of open files > _FS_SHARE",
"FR_INVALID_PARAMETER, Given parameter is invalid"};


void SD_FatFS_ShowFSError(unsigned char ucFSErrorCode)
{
	ShowOSTime(); printf("SD-ERROR: %s", strFSErrorCode[ucFSErrorCode]);
}

FRESULT SD_FatFS_Setup(void)
{ 
	SD_Error SD_Status;
	DWORD fre_clust;
	DWORD fre_sect, tot_sect;
	FRESULT res;
	
	pFS=&fstest;

	ShowOSTime(); printf("SDCard: Disk Initialization %u", (unsigned int)(SD_Status = (SD_Error)disk_initialize(0)));
  if (SD_Status != RES_OK) {
    ShowOSTime();  printf("SD Card Initial() was FAIL (%d)",SD_Status);        
    SD_FatFS_ShowFSError(SD_Status);
		return (FRESULT)SD_Status;
  }
	
	ShowOSTime(); printf("FATFS: Mount Disk %d", res = f_mount(0, &fs));
  if (res != FR_OK) {
    ShowOSTime(); printf("ERROR: f_mount() was FAIL (%d)",res);        
    SD_FatFS_ShowFSError(res);
		return res;
  }	

	
	res = f_getlabel("0:", testString, 0);
	ShowOSTime(); printf("FATFS: File System %s", strFSType[fs.fs_type-1]);	
	if (res != FR_OK) {
    ShowOSTime(); printf("ERROR: f_getlabel() was FAIL (%d)",res);        
    SD_FatFS_ShowFSError(res);
		return res;
  }
	ShowOSTime(); printf("FATFS: Volume Label \"%s\"", testString);

	res=f_getfree("0:", &fre_clust, &pFS);
	if (res != FR_OK) {
		ShowOSTime(); printf("ERROR: f_getfree() was FAIL (%d)",res);        
    SD_FatFS_ShowFSError(res);
		return res;
  }

  // Get total sectors and free sectors
  fre_sect = fre_clust * pFS->csize;
  tot_sect = (pFS->n_fatent - 2) * pFS->csize;

  // Print free space in unit of KB (assuming 512 bytes/sector) 
  ShowOSTime(); printf("FATFS: %lu KB Total Space.",tot_sect / 2);
  ShowOSTime(); printf("FATFS: %lu KB Available.", fre_sect / 2);

	return FR_OK;
}

/*******************************************************************************
* Function Name  : SetupADC1_DMA2
* Description    : 
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
#define ADC_BUFFERSIZE 8
uint16_t ADC_ConvValues[ADC_BUFFERSIZE];
#define ADC_DMA_Stream DMA2_Stream4
void ADC_DMA_Setup(void)
{
	ADC_InitTypeDef       ADC_InitStructure;
	ADC_CommonInitTypeDef ADC_CommonInitStructure;
	DMA_InitTypeDef       DMA_InitStructure;

	DMA_DeInit(ADC_DMA_Stream);
	
	/* DMA2 Stream0 channe0 configuration **************************************/
	DMA_InitStructure.DMA_Channel = DMA_Channel_0;  
	DMA_InitStructure.DMA_PeripheralBaseAddr = ((uint32_t)&ADC1->DR);
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)&ADC_ConvValues[0];
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
	DMA_InitStructure.DMA_BufferSize = ADC_BUFFERSIZE; // Count of 16-bit words
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	// DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(ADC_DMA_Stream, &DMA_InitStructure);
	/* DMA2_Stream0 enable */
	DMA_Cmd(ADC_DMA_Stream, ENABLE);
	
	/* Enable DMA Transfer Complete interrupt */
	// DMA_ITConfig(ADC1_Stream, DMA_IT_TC, ENABLE);

	/* ADC Common Init **********************************************************/
	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div8;
	ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_20Cycles;
	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADC_CommonInit(&ADC_CommonInitStructure);	
   
	/* ADC1 Init ****************************************************************/

	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
	ADC_InitStructure.ADC_ScanConvMode = ENABLE; // 1 Channel
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE; // Conversions Triggered
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
	ADC_InitStructure.ADC_ExternalTrigConv = 0;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfConversion = ADC_BUFFERSIZE;
	ADC_Init(ADC1,&ADC_InitStructure);


	ADC_TempSensorVrefintCmd(ENABLE);
	ADC_VBATCmd(DISABLE);
 
	/* ADC1 regular channel 10 configuration */
	// ADC_SampleTime_3Cycles
	// ADC_SampleTime_15Cycles  
	// ADC_SampleTime_28Cycles 
	// ADC_SampleTime_56Cycles   
	// ADC_SampleTime_84Cycles   
	// ADC_SampleTime_112Cycles  
	// ADC_SampleTime_144Cycles  
	// ADC_SampleTime_480Cycles  
 	ADC_RegularChannelConfig(ADC1, ADC_Channel_Vrefint, 1, ADC_SampleTime_480Cycles);
 	ADC_RegularChannelConfig(ADC1, ADC_Channel_11, 2, ADC_SampleTime_480Cycles);
 	ADC_RegularChannelConfig(ADC1, ADC_Channel_12, 3, ADC_SampleTime_480Cycles);
 	ADC_RegularChannelConfig(ADC1, ADC_Channel_13, 4, ADC_SampleTime_480Cycles);
 	ADC_RegularChannelConfig(ADC1, ADC_Channel_10, 5, ADC_SampleTime_480Cycles);
 	ADC_RegularChannelConfig(ADC1, ADC_Channel_6, 6, ADC_SampleTime_480Cycles);
 	ADC_RegularChannelConfig(ADC1, ADC_Channel_15, 7, ADC_SampleTime_480Cycles);
 	ADC_RegularChannelConfig(ADC1, ADC_Channel_14, 8, ADC_SampleTime_480Cycles);
 	//ADC_RegularChannelConfig(ADC1, ADC_Channel_TempSensor, 2, ADC_SampleTime_480Cycles);
 
	/* Enable ADC1 */
	ADC_Cmd(ADC1, ENABLE);
 
	/* Enable DMA request after last transfer (Single-ADC mode) */
	ADC_DMARequestAfterLastTransferCmd(ADC1, ENABLE);


	/* Enable DMA request after last transfer (Multi-ADC mode)  */
	// ADC_MultiModeDMARequestAfterLastTransferCmd(ENABLE);
 
	/* Enable ADC1 DMA */
	ADC_DMACmd(ADC1, ENABLE);

	/* Enable ADC1 */
	ADC_Cmd(ADC1, ENABLE);

	osDelay(3);
	ADC_SoftwareStartConv(ADC1);
}


static void DEBUG_UART_InitGPIO(void)
{
  DEBUG_UART_RCC_APBnPeriphClockCmd(DEBUG_UART_RCC_APBnPeriph_USART, ENABLE);
	
	DEBUG_UART_TX_RCC_APBnPeriphClockCmd(DEBUG_UART_TX_RCC_APBnPeriph_GPIO, ENABLE);
	DEBUG_UART_RX_RCC_APBnPeriphClockCmd(DEBUG_UART_RX_RCC_APBnPeriph_GPIO, ENABLE);

	GPIO_PinAFConfig(DEBUG_UART_TX_Port, DEBUG_UART_TX_SRC, DEBUG_UART_AF); //
	GPIO_PinAFConfig(DEBUG_UART_RX_Port, DEBUG_UART_RX_SRC, DEBUG_UART_AF);	
	
  GPIO_InitStructure.GPIO_Pin = DEBUG_UART_TX_Pin;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(DEBUG_UART_TX_Port, &GPIO_InitStructure);
 
  GPIO_InitStructure.GPIO_Pin = DEBUG_UART_RX_Pin;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(DEBUG_UART_RX_Port, &GPIO_InitStructure);	
}

// =========================================================================================
//	SetupDebugUART
// =========================================================================================
static void DEBUG_UART_Init(u32 u32baudrate)
{ 
  USART_InitStructure.USART_BaudRate = u32baudrate;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Tx | USART_Mode_Rx;
 
	USART_ITConfig(DEBUG_UART, USART_IT_RXNE, ENABLE);
	
  USART_Init(DEBUG_UART, &USART_InitStructure);

	USART_Cmd(DEBUG_UART, ENABLE);
}

void DEBUG_UART_Setup(u32 u32baudrate)
{ 
  DEBUG_UART_InitGPIO();
	DEBUG_UART_Init(u32baudrate);
}


/**
  * @brief  Retargets the C library printf function to the USART.
  * @param  None
  * @retval None
  */
int fputc(int ch, FILE *f)
{
  /* Place your implementation of fputc here */
  /* e.g. write a DEBUG_UART to the USART */
  USART_SendData(USART1, (uint8_t) ch);

  /* Loop until the end of transmission */
  while (USART_GetFlagStatus(DEBUG_UART, USART_FLAG_TC) == RESET)
  {}

  return ch;
}
