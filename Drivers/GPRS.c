#include "GPRS.h"

struct CLOCK_PROFILE GPRSClock;
char GPRSString[64];

//
// Definition for UART Debug Port
//
#define DEBUG_UART USART1
#define DEBUG_UART_AF GPIO_AF_USART1
#define DEBUG_UART_Baudrate 460800
#define DEBUG_UART_RCC_APBnPeriphClockCmd RCC_APB2PeriphClockCmd
#define DEBUG_UART_RCC_APBnPeriph_USART RCC_APB2Periph_USART1
#define DEBUG_UART_TX_RCC_APBnPeriphClockCmd RCC_APB1PeriphClockCmd
#define DEBUG_UART_TX_RCC_APBnPeriph_GPIO RCC_AHB1Periph_GPIOA
#define DEBUG_UART_TX_Port GPIOA
#define DEBUG_UART_TX_Pin GPIO_Pin_9
#define DEBUG_UART_TX_SRC GPIO_PinSource9
#define DEBUG_UART_RX_RCC_APBnPeriphClockCmd RCC_APB1PeriphClockCmd
#define DEBUG_UART_RX_RCC_APBnPeriph_GPIO RCC_AHB1Periph_GPIOA
#define DEBUG_UART_RX_Port GPIOA
#define DEBUG_UART_RX_Pin GPIO_Pin_10
#define DEBUG_UART_RX_SRC GPIO_PinSource10

#define GPRS_SHIFT_YEAR 26
#define GPRS_SHIFT_MONTH 22
#define GPRS_SHIFT_DAY 17
#define GPRS_SHIFT_HOUR 12
#define GPRS_SHIFT_MINUTE 6
#define GPRS_SHIFT_SECOND 0

#define GPRS_MASK_YEAR    0xFC000000
#define GPRS_MASK_MONTH   0x03C00000
#define GPRS_MASK_DAY     0x003E0000
#define GPRS_MASK_HOUR    0x0001F000
#define GPRS_MASK_MINUTE  0x00000FC0
#define GPRS_MASK_SECOND  0x0000003F

void GPRS_DecodeGPRSTimeValue(unsigned int uiCPRSTimeValue)
{  
	// 6(年), 4(月), 5(日), 5(時), 6(分), 6(秒)
	// 26, 22, 17, 12, 6, 0
  GPRSClock.usYear=(unsigned short)((uiCPRSTimeValue&GPRS_MASK_YEAR)>>GPRS_SHIFT_YEAR)+2000;
  GPRSClock.usMonth=(unsigned short)((uiCPRSTimeValue&GPRS_MASK_MONTH)>>GPRS_SHIFT_MONTH);
  GPRSClock.usDay=(unsigned short)((uiCPRSTimeValue&GPRS_MASK_DAY)>>GPRS_SHIFT_DAY);

  GPRSClock.usHour=(unsigned short)((uiCPRSTimeValue&GPRS_MASK_HOUR)>>GPRS_SHIFT_HOUR);
  GPRSClock.usMinute=(unsigned short)((uiCPRSTimeValue&GPRS_MASK_MINUTE)>>GPRS_SHIFT_MINUTE);
  GPRSClock.usSecond=(unsigned short)((uiCPRSTimeValue&GPRS_MASK_SECOND)>>GPRS_SHIFT_SECOND);
}

unsigned int GPRS_GetNowValue(void)
{
	RTC_GetDate(RTC_Format_BIN, &RTC_NowDateStructure);
	RTC_GetTime(RTC_Format_BIN, &RTC_NowTimeStructure);
  
	// 6(年), 4(月), 5(日), 5(時), 6(分), 6(秒)
	// 26, 22, 17, 12, 6, 0
	return ( ( RTC_NowDateStructure.RTC_Year)<<26  | 
					   RTC_NowDateStructure.RTC_Month<<22  | 
						 RTC_NowDateStructure.RTC_Date <<17 |
						 RTC_NowTimeStructure.RTC_Hours<<12 | 
						 RTC_NowTimeStructure.RTC_Minutes<<6 | 
					 ( RTC_NowTimeStructure.RTC_Seconds ) );
}

unsigned char GPRS_GetXORChecksum(char *cString)
{
	unsigned short i;
  unsigned char Checksum=cString[0];
  for(i=1;i<strlen(cString);i++) Checksum^=cString[i];
  return Checksum;
}

void MakeGPRSString(struct GPRS_PROFILE *GPRSData)
{
  sprintf(GPRSString, "$");
  sprintf(GPRSString, "%s%0.8X,", GPRSString, GPRSData->u32ID);
  sprintf(GPRSString, "%s%0.2X,", GPRSString, GPRSData->u8Area);
  sprintf(GPRSString, "%s%0.2X,", GPRSString, GPRSData->u8Pool);
  sprintf(GPRSString, "%s%0.1X,", GPRSString, GPRSData->u8EquipmentType);
  sprintf(GPRSString, "%s%0.1X,", GPRSString, GPRSData->u8EquipmentNumber);
  sprintf(GPRSString, "%s%0.4X,", GPRSString, GPRSData->s16EquipmentValue);
  sprintf(GPRSString, "%s%0.8X", GPRSString, GPRS_GetNowValue());
  sprintf(GPRSString, "%s*%0.2X\r\n", GPRSString, GPRS_GetXORChecksum(&GPRSString[1]));
}

// ------------------------------------------------------------------------------------------------------
//
//  GPRS Commnunication
//
//	 * Port: USART2
//	 * Baudrate: 57600, N, 8, 1
//
// ------------------------------------------------------------------------------------------------------
static void GPRS_InitGPIO(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
  DEBUG_UART_RCC_APBnPeriphClockCmd(DEBUG_UART_RCC_APBnPeriph_USART, ENABLE);
	
	DEBUG_UART_TX_RCC_APBnPeriphClockCmd(DEBUG_UART_TX_RCC_APBnPeriph_GPIO, ENABLE);
	DEBUG_UART_RX_RCC_APBnPeriphClockCmd(DEBUG_UART_RX_RCC_APBnPeriph_GPIO, ENABLE);

	GPIO_PinAFConfig(DEBUG_UART_TX_Port, DEBUG_UART_TX_SRC, DEBUG_UART_AF); //
	GPIO_PinAFConfig(DEBUG_UART_RX_Port, DEBUG_UART_RX_SRC, DEBUG_UART_AF);	
	
  GPIO_InitStructure.GPIO_Pin = DEBUG_UART_TX_Pin;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(DEBUG_UART_TX_Port, &GPIO_InitStructure);
 
  GPIO_InitStructure.GPIO_Pin = DEBUG_UART_RX_Pin;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_OD;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
  GPIO_Init(DEBUG_UART_RX_Port, &GPIO_InitStructure);	
}

static void GPRS_InitUART(u32 u32Baudrate)
{
  USART_InitTypeDef USART_InitStructure;

  // Enable USART2 clock                                          
  RCC_APB2PeriphClockCmd (RCC_APB2Periph_USART1, ENABLE);

  USART_InitStructure.USART_BaudRate            = u32Baudrate;
	USART_InitStructure.USART_WordLength          = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits            = USART_StopBits_1;
  USART_InitStructure.USART_Parity              = USART_Parity_No ;//USART_Parity_Odd
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;
 
  USART_Init(USART1, &USART_InitStructure);
	//	if you want to set the parity bit ,you must select 9bit data

	// Enable the USART1 Interrupt 
  //USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
 
	//GPIO_PinRemapConfig(GPIO_Remap_USART2, ENABLE);

  // Enable USART2 
  USART_Cmd(USART1, ENABLE);
}

void GPRS_Setup(u32 u32Baudrate)
{
  GPRS_InitGPIO();
  GPRS_InitUART(u32Baudrate);
}

void GPRS_PutChar(uint8_t u8Char)
{	
   USART_SendData(USART1, (uint8_t) u8Char);
   /* Loop until the end of transmission */
   while (USART_GetFlagStatus(USART1, USART_FLAG_TC) == RESET);
}

void GPRS_PutString(char *cString)
{	
  unsigned short i;

  for(i=0;i<strlen(cString);i++) GPRS_PutChar(cString[i]);
}


