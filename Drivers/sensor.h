#ifndef __SENSOR_H
#define __SENSOR_H

#include "config.h"

#define ADC_IntRefV 0
#define ADC_ExtRefT 1
#define ADC_VLIBAT 2
#define ADC_ExtRefV 3
#define ADC_SensorPressure 4
#define ADC_SensorIN1 5
#define ADC_SensorIN2 6
#define ADC_SensorIn3 7


float GetSystemVoltage(void);

#endif
