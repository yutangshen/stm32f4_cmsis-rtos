#ifndef __GPRS_H
#define __GPRS_H

#include "CLOCK.h"
#include <string.h>

struct GPRS_PROFILE
{
  unsigned  int   u32ID;
  unsigned  char  u8Area;
  unsigned  char  u8Pool;
  unsigned  char  u8EquipmentType;
  unsigned  char  u8EquipmentNumber;
            short s16EquipmentValue;
  unsigned  int   u32SyncTime;
};

extern struct CLOCK_PROFILE GPRSClock;
extern char GPRSString[];

void GPRS_DecodeGPRSTimeValue(unsigned int uiCPRSTimeValue);
unsigned int GPRS_GetNowValue(void);
void MakeGPRSString(struct GPRS_PROFILE *GPRSData);
void GPRS_Setup(u32 u32Baudrate);
void GPRS_PutChar(uint8_t u8Char);
void GPRS_PutString(char *cString);

#endif
