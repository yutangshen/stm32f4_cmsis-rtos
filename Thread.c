
#include "cmsis_os.h"                                           // CMSIS RTOS header file
#include "config.h"
#include "LED.h"

/*----------------------------------------------------------------------------
 *      Thread 1 'Thread_Name': Sample thread
 *---------------------------------------------------------------------------*/
 
void Thread_LEDRun (void const *argument);                           		// thread function
osThreadId tid_Thread;                                          				// thread id
osThreadDef (Thread_LEDRun, osPriorityNormal, 1, 0);                   	// thread object

int Init_Thread (void) {

  tid_Thread = osThreadCreate (osThread(Thread_LEDRun), NULL);
  if (!tid_Thread) return(-1);
  
  return(0);
}

void Thread_LEDRun (void const *argument) 
{
	unsigned short i;
  while (1) {
    // Insert thread code here...
		for(i=0;i<usLedNumber;i++) {
			LED_On(i);
			osDelay(500);
			LED_Off(i);
			osDelay(500);
		}
    osThreadYield ();                                           // suspend thread
  }
}
