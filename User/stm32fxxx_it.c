/**
  ******************************************************************************
  * @file    I2C/EEPROM/stm32f4xx_it.c 
  * @author  MCD Application Team
  * @version V1.0.0
  * @date    30-September-2011
  * @brief   Main Interrupt Service Routines.
  *          This file provides template for all exceptions handler and 
  *          peripherals interrupt service routine.
  ******************************************************************************
  * @attention
  *
  * THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
  * WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE
  * TIME. AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY
  * DIRECT, INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING
  * FROM THE CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE
  * CODING INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
  *
  * <h2><center>&copy; COPYRIGHT 2011 STMicroelectronics</center></h2>
  ******************************************************************************
  */ 

/* Includes ------------------------------------------------------------------*/
#include "stm32fxxx_it.h"
#include "config.h"
#include "sdcard.h"
#include "ads129x.h"
#include "SRAM.h"

extern struct OP_SYSTEM OP_STATUS;


//#define USARTx_IRQHANDLER   USART3_IRQHandler
/** @addtogroup STM32F4xx_StdPeriph_Examples
  * @{
  */

/** @addtogroup I2C_EEPROM
  * @{
  */ 

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

/******************************************************************************/
/*            Cortex-M4 Processor Exceptions Handlers                         */
/******************************************************************************/

/**
  * @brief   This function handles NMI exception.
  * @param  None
  * @retval None
  */
void NMI_Handler(void)
{
}

/**
  * @brief  This function handles Hard Fault exception.
  * @param  None
  * @retval None
  */
void HardFault_Handler(void)
{
  /* Go to infinite loop when Hard Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Memory Manage exception.
  * @param  None
  * @retval None
  */
void MemManage_Handler(void)
{
  /* Go to infinite loop when Memory Manage exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Bus Fault exception.
  * @param  None
  * @retval None
  */
void BusFault_Handler(void)
{
  /* Go to infinite loop when Bus Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles Usage Fault exception.
  * @param  None
  * @retval None
  */
void UsageFault_Handler(void)
{
  /* Go to infinite loop when Usage Fault exception occurs */
  while (1)
  {
  }
}

/**
  * @brief  This function handles SVCall exception.
  * @param  None
  * @retval None
  */
void SVC_Handler(void)
{
}

/**
  * @brief  This function handles Debug Monitor exception.
  * @param  None
  * @retval None
  */
void DebugMon_Handler(void)
{
}

/**
  * @brief  This function handles PendSVC exception.
  * @param  None
  * @retval None
  */
void PendSV_Handler(void)
{
}

/**
  * @brief  This function handles SysTick Handler.
  * @param  None
  * @retval None
  */
uint32_t uiADCTemp1;
uint32_t uiADCTemp2;
#define AD_RefV_Factor 128
#define AD_LiBAT_Factor 32
void SysTick_Handler(void)
{	
	OP_STATUS.uiSysTickCounter++;
	OP_STATUS.uiDelayCounter++;
	
	uiADCTemp1+=ADC1_ConvValues[AD1_VREF];
	if((OP_STATUS.uiSysTickCounter%AD_RefV_Factor)==0) {
		OP_STATUS.fADCReference1V2=(double)uiADCTemp1/(double)AD_RefV_Factor;
		uiADCTemp1=0;
	}

	uiADCTemp2+=ADC1_ConvValues[AD1_VLIBAT];
	if((OP_STATUS.uiSysTickCounter%AD_LiBAT_Factor)==0) {
		OP_STATUS.fADCLiBatteryV=(double)uiADCTemp2/(double)AD_LiBAT_Factor;
		uiADCTemp2=0;
		
		if(OP_STATUS.bShutdown==false) {
			if(BT_Check()) {
				OP_STATUS.uiCheckBatteryCounter++;
			} else {
				OP_STATUS.uiCheckBatteryCounter=0;
			}
			if(OP_STATUS.uiCheckBatteryCounter>128) OP_STATUS.bShutdown=true;
		}
	}
	
	if(OP_STATUS.bAlarm) {
		if((OP_STATUS.uiSysTickCounter/128)&0x1) {
			LedAlarmOn();
		} else {
			LedAlarmOff();
		}
	}
}

/******************************************************************************/
/*                 STM32F4xx Peripherals Interrupt Handlers                   */
/*  Add here the Interrupt Handler for the used peripheral(s) (PPP), for the  */
/*  available peripheral interrupt handler's name please refer to the startup */
/*  file (startup_stm32f4xx.s).                                               */
/******************************************************************************/
void USART1_IRQHandler(void)
{
	u8 		rx_data;
	u16		u16Select;
	
	if(USART_GetITStatus(USART1, USART_IT_RXNE) != RESET)
	{ 
  		USART_ClearITPendingBit(USART1,USART_IT_RXNE);
		rx_data=(u8)USART_ReceiveData(USART1);
		u16Select=OP_STATUS.usRx1Select&1;
		if(rx_data=='$') {
			Rx1Buffer[u16Select].usRxIndex=0;
			//Rx1Buffer[u16Select].cCMDString[Rx1Buffer[u16Select].usRxIndex]='$';
		} else if(rx_data=='*') {
			Rx1Buffer[u16Select].cCMDString[Rx1Buffer[u16Select].usRxIndex]='\0';
			Rx1Buffer[u16Select].usLength=Rx1Buffer[u16Select].usRxIndex;
			CheckRx1CommandRESET(Rx1Buffer[u16Select].cCMDString);
			Rx1Buffer[u16Select].bCheck=true;		
			OP_STATUS.usRx1Select++;
			OP_STATUS.usRx1Select&=1;
			Rx1Buffer[OP_STATUS.usRx1Select].usRxIndex=0;
		} else {
			Rx1Buffer[u16Select].cCMDString[Rx1Buffer[u16Select].usRxIndex++]=rx_data;
		}
	}
}

/*******************************************************************************
* Function Name  : USART2_IRQHandler
* Description    : This function handles USART2 global interrupt request.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void USART2_IRQHandler(void)
{
	u8 		rx_data;

	if(USART_GetITStatus(USART2, USART_IT_RXNE) != RESET)
	{ 
  		USART_ClearITPendingBit(USART2,USART_IT_RXNE);
		rx_data=(u8)USART_ReceiveData(USART2);
		Rx2Buffer.cCMDString[Rx2Buffer.usRxIndex++]=rx_data;
	}
}

/*******************************************************************************
* Function Name  : SDIO_IRQHandler
* Description    : This function handles SDIO global interrupt request.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SDIO_IRQHandler(void)
{
  /* Process All SDIO Interrupt Sources */
  SD_ProcessIRQSrc();
}

/*******************************************************************************
* Function Name  : RTC_Alarm_IRQHandler
* Description    : This function handles RTC global interrupt request.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void RTC_Alarm_IRQHandler(void)
{
  if(RTC_GetITStatus(RTC_IT_ALRA) != RESET)
  {
	 RTC_ClearITPendingBit(RTC_IT_ALRA);
	 EXTI_ClearITPendingBit(EXTI_Line17);
  } 
}
void EXTI2_IRQHandler(void)
{
	
}

u32 TestIndex;
void EXTI9_5_IRQHandler(void)
{
	u32 i;
	u32 u32DataIndex;
    u32 u32DataTemp;
    s16 s16PDMTemp1,s16PDMTemp2;

    TestIndex++;
    /*
        if(TestIndex&0x1) {
            LedAlarmOn();
        } else {
            LedAlarmOff();
        }
    */
    if(EXTI_GetITStatus(ADS129x_DRDY_EXTI_Line)!=RESET) { 
        EXTI_ClearITPendingBit(ADS129x_DRDY_EXTI_Line);
            
		if(OP_STATUS.bCaptureStart) {
			OP_STATUS.uiAFEDRDYCount++;
            if(OP_STATUS.bAlarm==false) LedAlarmOn();
			
            // PCG Sampling
            if(OP_STATUS.bPCGInited) {
                s16PDMTemp1=0;
                s16PDMTemp2=0;
                u32DataIndex=OP_STATUS.uiPCGDataIndex&0x1;
                for(i=0;i<PDMBufferSize;i++) {
                    s16PDMTemp1+=(s16)PDMtoPCMTable[ucPDMBuffer[0][i]];
                    s16PDMTemp2+=(s16)PDMtoPCMTable[ucPDMBuffer[1][i]];
                }
                u32DataTemp=2*OP_STATUS.uiPCGDataCount;
                SRAM_s16Write(ExtSRAM_PCG[0].u32Start[u32DataIndex]+u32DataTemp,(s16PDMTemp1-PDMBufferShift)*PDMBufferTimes);
                SRAM_s16Write(ExtSRAM_PCG[1].u32Start[u32DataIndex]+u32DataTemp,(s16PDMTemp2-PDMBufferShift)*PDMBufferTimes);
                OP_STATUS.uiPCGDataCount++;
                if(OP_STATUS.uiPCGDataCount>=OP_STATUS.uiPCGDataSize) {
                    OP_STATUS.uiPCGDataCount=0;
                    OP_STATUS.uiPCGDataIndex++;
                    OP_STATUS.uiPCGDataIndex&=0x1;
                    OP_STATUS.bSavePCG=true;
                }
			}
            
			// ECG Sampling
			if((OP_STATUS.uiAFEDRDYCount%ADS129x.uiDataSamplingFactor)==0) {
                ADS129x_ReadDATA();

				u32DataIndex=OP_STATUS.uiECGDataIndex&0x1;
                u32DataTemp=OP_STATUS.uiECGUniPacketSize*OP_STATUS.uiECGDataCount;
				for(i=0;i<ADS129x.uiDataNumber;i++) {
                    SRAM_s32Write(ExtSRAM_ECG.u32Start[u32DataIndex]+u32DataTemp+4*i,ADS129x_RxBuffer.s32Data[i]);
                    //ECGBuffer[u32DataIndex][OP_STATUS.uiECGDataCount].iValue[i]=ADS129x_RxBuffer.s32Data[i];
                }
				OP_STATUS.uiECGDataCount++;
				if(OP_STATUS.uiECGDataCount>=OP_STATUS.uiECGDataSize) {
					OP_STATUS.uiECGDataCount=0;
					OP_STATUS.uiECGDataIndex++;
					OP_STATUS.uiECGDataIndex&=0x1;
					OP_STATUS.bSaveECG=true;
				}
			}

			// ACC Sampling
            if(OP_STATUS.bACCInited) {
                if((OP_STATUS.uiAFEDRDYCount%ADXL337.uiDataSamplingFactor)==0) {
                    u32DataIndex=OP_STATUS.uiACCDataIndex&0x1;
                    u32DataTemp=OP_STATUS.uiACCUniPacketSize*OP_STATUS.uiACCDataCount;
                    SRAM_u16Write(ExtSRAM_ACC.u32Start[u32DataIndex]+u32DataTemp+0,ADC1_ConvValues[AD1_ACC_X]);
                    SRAM_u16Write(ExtSRAM_ACC.u32Start[u32DataIndex]+u32DataTemp+2,ADC1_ConvValues[AD1_ACC_Y]);
                    SRAM_u16Write(ExtSRAM_ACC.u32Start[u32DataIndex]+u32DataTemp+4,ADC1_ConvValues[AD1_ACC_Z]);
                    OP_STATUS.uiACCDataCount++;
                    if(OP_STATUS.uiACCDataCount>=OP_STATUS.uiACCDataSize) {
                        OP_STATUS.uiACCDataCount=0;
                        OP_STATUS.uiACCDataIndex++;
                        OP_STATUS.uiACCDataIndex&=0x1;
                        OP_STATUS.bSaveACC=true;

                    }
				} 
			}
			
            if(OP_STATUS.bAlarm==false) LedAlarmOff();
			
			if((OP_STATUS.uiAFEDRDYCount/OP_STATUS.uiLEDFlashSeed)&0x1) {
				LedStatusOff();
			} else {
				LedStatusOn();
			}
		}
	}
}

void DMA2_Stream3_IRQHandler(void) // Called at 1 KHz for 200 KHz sample rate, LED Toggles at 500 Hz
{

	SD_ProcessDMAIRQ();
}


/**
  * @brief  This function handles PPP interrupt request.
  * @param  None
  * @retval None
  */
/*void PPP_IRQHandler(void)
{
}*/

/**
  * @}
  */ 

/**
  * @}
  */ 

/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
