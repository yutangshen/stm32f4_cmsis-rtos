#ifndef _USART_H
#define _USART_H

#include <stdio.h>
#include <string.h>
#include "stm32f4xx.h"

void SetupUSART1(u32 u32Baudrate);
void SetupUSART2(u32 u32Baudrate);
void SendChar2(uint8_t u8Char);
void SendString2(char *cString);

#endif /*_USART_H*/
