/**
  ******************************************************************************
  * @file    rtc.c
  * @author  ITRI South Campus MSTC Healthcare Team (R.O.C)
  * @version V1.0.1
  * @date    11-November-2013
  * @brief   RTC functions
  ******************************************************************************
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "rtc.h"
#include "config.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
RTC_TimeTypeDef RTC_TimeStructure;
RTC_DateTypeDef RTC_DateStructure;
RTC_TimeTypeDef RTC_SetTimeStructure;
RTC_DateTypeDef RTC_SetDateStructure;
RTC_TimeTypeDef RTC_NowTimeStructure;
RTC_DateTypeDef RTC_NowDateStructure;
RTC_TimeTypeDef RTC_TimeStampStructure;
RTC_DateTypeDef RTC_TimeStampDateStructure;
__IO uint32_t AsynchPrediv = 0, SynchPrediv = 0;

char TimeString[32];

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
// ========================================================================================================================
//  RTC PArt
// ========================================================================================================================
void SetupRTC(void) 
{
	RTC_InitTypeDef   	RTC_InitStructure;
	
	if (RTC_ReadBackupRegister(RTC_BKP_DR0) != STD_BKP_DR0)
	{
		/* RTC configuration  */
    	printf("\r\n => RTC not yet configured ...");
		InitRTC();
    	printf("\r\n => RTC Configuring ...");

		/* Configure the RTC data register and RTC prescaler */
		RTC_InitStructure.RTC_AsynchPrediv = AsynchPrediv;
		RTC_InitStructure.RTC_SynchPrediv = SynchPrediv;
		RTC_InitStructure.RTC_HourFormat = RTC_HourFormat_24;
   
		/* Check on RTC init */
		if (RTC_Init(&RTC_InitStructure) == ERROR) {
			printf("\r\n => RTC Prescaler Config failed\r\n");
		} else {
			printf("\r\n => Success.\r\n");			
		}

		/* Configure the time register */
		RTC_TimeStructure.RTC_Hours=DEF_TIME_HOUR;
		RTC_TimeStructure.RTC_Minutes=DEF_TIME_MINUTE;
		RTC_TimeStructure.RTC_Seconds=DEF_TIME_SECOND;
		
		RTC_DateStructure.RTC_Year = DEF_DATE_YEAR;
		RTC_DateStructure.RTC_Month = DEF_DATE_MONTH;
		RTC_DateStructure.RTC_Date = DEF_DATE_DAY;
		RTC_DateStructure.RTC_WeekDay = DEF_DATE_WEEKDAY;

		/* Configure the RTC date register */
		if(RTC_SetDate(RTC_Format_BIN, &RTC_DateStructure) == ERROR) {
			printf("    >> RTC Set Date failed. !! <<\r\n");
		} else {
			printf("    >> RTC Set Date success: ");
			RTC_ShowDate();
			printf("\r\n");
			/* Indicator for the RTC configuration */
			RTC_WriteBackupRegister(RTC_BKP_DR0, STD_BKP_DR0);
		}

		/* Configure the RTC time register */
		if(RTC_SetTime(RTC_Format_BIN, &RTC_TimeStructure) == ERROR) {
			printf("    >> RTC Set Time failed.\r\n");
		} else {
			printf("    >> RTC Set Time success: ");
			RTC_ShowTime();
			printf("\r\n");
			/* Indicator for the RTC configuration */
			RTC_WriteBackupRegister(RTC_BKP_DR0, STD_BKP_DR0);
		}
	} else {
		/* Check if the Power On Reset flag is set */
		if (RCC_GetFlagStatus(RCC_FLAG_PORRST) != RESET) {	
			printf("\r\n => Power On Reset occurred....");
		/* Check if the Pin Reset flag is set */
		} else if (RCC_GetFlagStatus(RCC_FLAG_PINRST) != RESET) {
			printf("\r\n => External Reset occurred....");
		}

		printf("\r\n => No need to configure RTC....");
    
		/* Enable the PWR clock */
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

		/* Allow access to RTC */
		PWR_BackupAccessCmd(ENABLE);

		/* Wait for RTC APB registers synchronisation */
		RTC_WaitForSynchro();


		/* Clear the RTC Alarm Flag */
		RTC_ClearFlag(RTC_FLAG_ALRAF);

		/* Clear the EXTI Line 17 Pending bit (Connected internally to RTC Alarm) */
		EXTI_ClearITPendingBit(EXTI_Line17);

	}
	
}

void InitRTC(void)
{
  /* Enable the PWR clock */
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_PWR, ENABLE);

  /* Allow access to RTC */
  PWR_BackupAccessCmd(ENABLE);

#if defined (RTC_CLOCK_SOURCE_LSI)  /* LSI used as RTC source clock*/
/* The RTC Clock may varies due to LSI frequency dispersion. */
  /* Enable the LSI OSC */ 
  RCC_LSICmd(ENABLE);

  /* Wait till LSI is ready */  
  while(RCC_GetFlagStatus(RCC_FLAG_LSIRDY) == RESET)
  {
  }

  /* Select the RTC Clock Source */
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSI);

  SynchPrediv = 0xFF;
  AsynchPrediv = 0x7F;

#elif defined (RTC_CLOCK_SOURCE_LSE) /* LSE used as RTC source clock */
  /* Enable the LSE OSC */
  RCC_LSEConfig(RCC_LSE_ON);

  /* Wait till LSE is ready */  
  while(RCC_GetFlagStatus(RCC_FLAG_LSERDY) == RESET)
  {
  }

  /* Select the RTC Clock Source */
  RCC_RTCCLKConfig(RCC_RTCCLKSource_LSE);

  SynchPrediv = 0xFF;
  AsynchPrediv = 0x7F;
    
#else
  #error Please select the RTC Clock source inside the main.c file
#endif /* RTC_CLOCK_SOURCE_LSI */

  /* Enable the RTC Clock */
  RCC_RTCCLKCmd(ENABLE);

  /* Wait for RTC APB registers synchronisation */
  RTC_WaitForSynchro();

  /* Enable The TimeStamp */
  RTC_TimeStampCmd(RTC_TimeStampEdge_Falling, ENABLE);    
}

void RTC_ShowDate(void)
{
	/* Get the current Date */
	RTC_GetDate(RTC_Format_BIN, &RTC_NowDateStructure);
	printf("20%0.2d/%0.2d/%0.2d ", RTC_NowDateStructure.RTC_Year, RTC_NowDateStructure.RTC_Month, RTC_NowDateStructure.RTC_Date);
}

void RTC_ShowTime(void)
{
	/* Get the current Time and Date */
	RTC_GetTime(RTC_Format_BIN, &RTC_NowTimeStructure);
	printf("%0.2d:%0.2d:%0.2d ", RTC_NowTimeStructure.RTC_Hours, RTC_NowTimeStructure.RTC_Minutes, RTC_NowTimeStructure.RTC_Seconds);
	/* Unfreeze the RTC DR Register */
	//(void)RTC->DR;
}

void RTC_ShowNow(void)
{
	/* Get the current Date */
	RTC_GetDate(RTC_Format_BIN, &RTC_NowDateStructure);
	printf("\r\n");
	RTC_GetDate(RTC_Format_BIN, &RTC_NowDateStructure);
	printf("20%0.2d/%0.2d/%0.2d ", RTC_NowDateStructure.RTC_Year, RTC_NowDateStructure.RTC_Month, RTC_NowDateStructure.RTC_Date);
	RTC_GetTime(RTC_Format_BIN, &RTC_NowTimeStructure);
	printf("%0.2d:%0.2d:%0.2d ", RTC_NowTimeStructure.RTC_Hours, RTC_NowTimeStructure.RTC_Minutes, RTC_NowTimeStructure.RTC_Seconds);
}

void RTC_GetDateTimeString(void) 
{
    RTC_GetDate(RTC_Format_BIN, &RTC_NowDateStructure);
	RTC_GetTime(RTC_Format_BIN, &RTC_NowTimeStructure);
    RTC_GetDate(RTC_Format_BIN, &RTC_NowDateStructure);
    sprintf(TimeString,"20%0.2d/%0.2d/%0.2d %0.2d:%0.2d:%0.2d", RTC_NowDateStructure.RTC_Year, RTC_NowDateStructure.RTC_Month, RTC_NowDateStructure.RTC_Date,RTC_NowTimeStructure.RTC_Hours, RTC_NowTimeStructure.RTC_Minutes, RTC_NowTimeStructure.RTC_Seconds);
}

void RTC_SetDateTime(void)
{	
	// Configure the RTC date register */
	if(RTC_SetDate(RTC_Format_BIN, &RTC_SetDateStructure) == ERROR) {
		printf("\r\n    >> RTC Set Date failed. !!");
	} else {
		RTC_WriteBackupRegister(RTC_BKP_DR0, STD_BKP_DR0);
	}

	// Configure the RTC time register 
	if(RTC_SetTime(RTC_Format_BIN, &RTC_SetTimeStructure) == ERROR) {
		printf("\r\n    >> RTC Set Time failed.\r\n");
	} else {
		RTC_WriteBackupRegister(RTC_BKP_DR0, STD_BKP_DR0);
	}
	
	//
	do {
		Delay(100);
		RTC_GetDate(RTC_Format_BIN, &RTC_DateStructure);
	} while(RTC_DateStructure.RTC_Date!=RTC_SetDateStructure.RTC_Date);
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
