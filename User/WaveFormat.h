#ifndef __WAVEFORMAT_H
#define __WAVEFORMAT_H

struct WAVE_HEAD {
	char 			ChunkID[4]; 	// 0 ~ 3
	unsigned int 	ChunkSize;		// 4 ~ 7
	char			Format[4];		// 8 ~ 11
	char 			SubChunk1ID[4];	// 12 ~ 15
	unsigned int 	SubChunk1Size;	// 16 ~ 19
	unsigned short 	AudioFormat;	// 20 ~ 21
	unsigned short 	NumChannels;	// 22 ~ 23
	unsigned int 	SampleRate;		// 24 ~ 27
	unsigned int 	ByteRate;		// 28 ~ 31
	unsigned short 	BolckAlign;		// 32 ~ 33
	unsigned short  BitPerSample; 	// 34 ~ 35
	char			SubChunk2ID[4];	// 36 ~ 39
	unsigned int	SubChunk2Size;	// 40 ~ 43
};
#endif
