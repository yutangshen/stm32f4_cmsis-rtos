#ifndef __SRAM_H
#define __SRAM_H

#include "stm32f4xx.h"
#include "config.h"


#define SRAM_MAX_ADDR 0x001FFFFF // 2097152
#define Bank1_SRAM1_ADDR 0x60000000
#define Bank1_SRAM2_ADDR 0x64000000
#define Bank1_SRAM3_ADDR 0x68000000
#define Bank1_SRAM4_ADDR 0x6B000000

#define ExtSRAMBase Bank1_SRAM1_ADDR

#define SRAM_Write(addr,data) ((*(uint16_t *) (ExtSRAMBase + addr)) = data)
#define SRAM_Read(addr) (*(uint16_t*) (ExtSRAMBase + addr ))

#define SRAM_s16Write(addr,data) ((*(int16_t *) (ExtSRAMBase + addr)) = data)
#define SRAM_s16Read(addr) (*(int16_t*) (ExtSRAMBase + addr ))
#define SRAM_s32Write(addr,data) ((*(int32_t *) (ExtSRAMBase + addr)) = data)
#define SRAM_s32Read(addr) (*(int32_t*) (ExtSRAMBase + addr ))

#define SRAM_u16Write(addr,data) ((*(uint16_t *) (ExtSRAMBase + addr)) = data)
#define SRAM_u16Read(addr) (*(uint16_t*) (ExtSRAMBase + addr ))
#define SRAM_u32Write(addr,data) ((*(uint32_t *) (ExtSRAMBase + addr)) = data)
#define SRAM_u32Read(addr) (*(uint32_t*) (ExtSRAMBase + addr ))

// ========================================================================================
//
// ========================================================================================
void SRAM_Init(void);
void SRAM_Test(u32 u32Count);
//void SRAM_s32Write(u32 addr,s32 s32Value);
//s32 SRAM_s32Read(u32 addr);

#endif
