/**
  ******************************************************************************
  * @file    rtc.h
  * @author  MCD Application Team
  * @version V1.0.1
  * @date    11-November-2013
  * @brief   Header for rtc module
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __RTC_H
#define __RTC_H

/* Includes ------------------------------------------------------------------*/
#include "stm32f4xx_rtc.h"
//#include "global_includes.h"
/* Exported types ------------------------------------------------------------*/
#define RTC_CLOCK_SOURCE_LSE

#define STD_BKP_DR0 0xF5F5
#define DEF_DATE_YEAR 14
#define DEF_DATE_MONTH 11
#define DEF_DATE_DAY 11
#define DEF_DATE_WEEKDAY 2
#define DEF_TIME_HOUR 11
#define DEF_TIME_MINUTE 11
#define DEF_TIME_SECOND 11
/* Exported constants --------------------------------------------------------*/
extern char TimeString[32];
extern RTC_TimeTypeDef RTC_TimeStructure;
extern RTC_DateTypeDef RTC_DateStructure;
extern RTC_TimeTypeDef RTC_SetTimeStructure;
extern RTC_DateTypeDef RTC_SetDateStructure;
extern RTC_TimeTypeDef RTC_NowTimeStructure;
extern RTC_DateTypeDef RTC_NowDateStructure;
/* Exported macro ------------------------------------------------------------*/
/* Exported functions ------------------------------------------------------- */
void SetupRTC(void);
void InitRTC(void);
void RTC_GetDateTimeString(void);
void RTC_ShowDate(void);
void RTC_ShowTime(void);
void RTC_ShowNow(void);
void RTC_SetDateTime(void);


#endif /* __APP_RTC_H */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
