#ifndef __CONFIG_H
#define __CONFIG_H

#include "stm32f4xx.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include "ff.h"

#define SAVE_ECG_DATA
#define SAVE_PCG_DATA
#define SAVE_ACC_DATA

#define ADC_VLIBAT ADC_Channel_10
#define ADC_ACC_X ADC_Channel_14
#define ADC_ACC_Y ADC_Channel_13
#define ADC_ACC_Z ADC_Channel_11

#define VERSION "0.1g"

#define AD1_ACC_X 2
#define AD1_ACC_Y 3
#define AD1_ACC_Z 4
#define AD1_VREF 0
#define AD1_VLIBAT 1

#define ButtonDelay 200

#define DEF_STRING_LENGTH 64

#define PWR_MCU GPIO_Pin_2
#define PWR_SD GPIO_Pin_1

#define SysTickFrequency 1000

#define PowerOn() { GPIO_SetBits(GPIOI,PWR_MCU); }
#define PowerOff() { GPIO_ResetBits(GPIOI,PWR_MCU); }
#define SDCardOn() { GPIO_SetBits(GPIOI,PWR_SD); }
#define SDCardOff() { GPIO_ResetBits(GPIOI,PWR_SD); }
#define AccSelfTestOn() { GPIO_SetBits(GPIOH,GPIO_Pin_4); }
#define AccSelfTestOff() { GPIO_ResetBits(GPIOH,GPIO_Pin_4); }
#define BluetoothOn() {	GPIO_SetBits(GPIOH, GPIO_Pin_12); }
#define BluetoothOff() { GPIO_ResetBits(GPIOH, GPIO_Pin_12); }

#define LedAlarmOn() { GPIO_ResetBits(GPIOF,GPIO_Pin_7); }
#define LedAlarmOff() { GPIO_SetBits(GPIOF,GPIO_Pin_7); }
#define LedAccessOn() { GPIO_ResetBits(GPIOF,GPIO_Pin_6); }
#define LedAccessOff() { GPIO_SetBits(GPIOF,GPIO_Pin_6); }
#define LedStatusOn() { GPIO_ResetBits(GPIOF,GPIO_Pin_10); }
#define LedStatusOff() { GPIO_SetBits(GPIOF,GPIO_Pin_10); }

#define LedAllOff(); { LedStatusOff(); LedAccessOff(); LedAlarmOff(); }
#define LedAllOn(); { LedStatusOn(); LedAccessOn(); LedAlarmOn(); }

#define CardReader_Disable() { GPIO_ResetBits(GPIOG,GPIO_Pin_13); }
#define CardReader_Enable() { GPIO_SetBits(GPIOG,GPIO_Pin_13); }

#define BT_Check() (GetBatteryVoltage()>(float)5.5 ? true : false)

#define NbrOfPCG 2

#define SaveSecond 5
#define SaveLimit 720 // 1 for 3s, 20 for 1min, 100 for 5min, 400 for 20min

#define ECGSamplingRate 200
#define ACCSamplingRate 100
#define PCGChannel 2

#define ECGResp 0
#define ECGLead1 1  
#define ECGLead2 3  
#define ECGLead3 2 

#define TestTypeNon 0
#define TestTypeECG 1
#define TestTypeACC 2
#define TestTypePCG 3
#define TestTypeBLUETOOTH 4
#define TestTypeSetBLUETOOTH 5
#define TestTypeLED 6
#define TestTypeFileSystem 7

struct OP_SYSTEM {	
	unsigned int 	uiSysTickCounter;
	bool			bSysTickEnabled;
	bool			bCheckBT;
	bool			bAlarm;
	bool			bShutdown;
	
	unsigned int	uiSaveFileCheckBatteryIndex;
	unsigned int	uiCheckBatteryCounter;
	
	bool			bSRAMInited;
	
	unsigned int 	uiDelayCounter;
	
	unsigned int 	uiLEDFlashSeed;

	unsigned int 	uiDelaySeed;
		
	float 			fADCReference1V2;
	float 			fADCLiBatteryV;

	bool			bCaptureStart;
	unsigned int	uiAFESamplingRate;
	unsigned int 	uiAFEDRDYCount;
    unsigned int    uiExtSRAMTotalUsed;
	
	unsigned short	usRx1Select;
	
	// Test Mode Flag
	unsigned char	ucTestType;
	
	// System Parameter for ECG Capture Operating
	bool			bECGInited;
	bool			bSaveECG;
	unsigned int	uiECGDataSize;				// SaveSecond*SamplingRate
	unsigned int 	uiECGDataCount;				// 接收資料的計數器
	unsigned char 	uiECGDataIndex;				// 接收資料的區塊 
	unsigned int 	uiECGUniPacketSize;			// 每次 Sampling 的資料大小, ADS1298 有 27=9*3

	// System Parameter for ECG Capture Operating 
	bool			bACCInited;
	bool			bSaveACC;
	unsigned int	uiACCDataSize;				// SaveSecond*SamplingRate
	unsigned int 	uiACCDataCount;				// 接收資料的計數器
	unsigned char 	uiACCDataIndex;				// 接收資料的區塊 
	unsigned int 	uiACCUniPacketSize;			// 每次 Sampling 的資料大小, ADS1298 有 27=9*3

	// System Parameter for PCG Capture Operating
	bool			bPCGInited;
	bool			bSavePCG;
	unsigned int	uiPCGDataSize;				// SaveSecond*SamplingRate*sizeof(short);
	unsigned int 	uiPCGDataCount;				// 接收資料的計數器
	unsigned char 	uiPCGDataIndex;				// 接收資料的區塊 
	unsigned int 	uiPCGUniPacketSize;			// 每次 Sampling 的資料大小, ADS1298 有 27=9*3	
	
};

struct ECG_BUFFER
{
	// --------------------------------------------------------
	// 0: Respiration
	// 1: Lead I (1)
	// 2: Lead III (3)
	// 3: Lead II (2)
	// --------------------------------------------------------
	int iValue[4];
};

struct ACC_BUFFER	
{
	// --------------------------------------------------------
	// 0: ACC_X
	// 1: ACC_Y
	// 2: ACC_Z
	// --------------------------------------------------------
	unsigned short usValue[3];
};


struct ACC_PROFILE {
	unsigned int 	uiDataNumber;
	unsigned int  	uiDataSamplingRate;
	unsigned int 	uiDataSamplingFactor;
	float 			fValue0G[3];
	float			fValuePerG[3];
};

struct PCG_PROFILE {
	unsigned int  	uiDataSamplingRate;
	unsigned int 	uiDataNumber;
};

struct SAVE_FILE {
	char  	strFilename[DEF_STRING_LENGTH];
	FIL 	fdst;
	
	u32		u32SaveCount;
	u32 	u32SaveIndex;
	u32		u32SaveBufferSize;
	u32		u32BytesWrite;
	u32		u32CountWrite;
	u32		u32TotalWrite;
};

struct ExtSRAM_PROFILE {
    u32     u32Start[2];
    u32     u32Size[2];
    u32     u32TotalSize;
};

struct LED_PROFILE {
	bool	bLedAlarm;
	bool	bLedAccess;
	bool	bLedStatus;	
};

struct BLUETOOTH_PROFILE {
	char 	strAddressBLE[32];
	char 	strHostnameBLE[32];
	char 	strAddressSPP[32];
	char 	strHostnameSPP[32];
};

#define USART_NbrOfBuffer 2
struct USART_BUFFER {
	bool			bCheck;
	unsigned short	usRxIndex;
	unsigned short	usLength;
	char 			cCMDString[DEF_STRING_LENGTH];
};

#define PDMBufferSize 128
#define PDMBufferShift (4*PDMBufferSize)
//#define PDMBufferShift 0
#define PDMBufferTimes (16384/PDMBufferSize)

extern uint16_t ADC1_ConvValues[];
extern uint16_t ADC3_ConvValues[];
extern struct OP_SYSTEM OP_STATUS;
extern struct USART_BUFFER Rx1Buffer[USART_NbrOfBuffer];
extern struct USART_BUFFER Rx2Buffer;
extern short PDMtoPCMTable[256];

extern struct ECG_BUFFER ECGBuffer[2][SaveSecond*ECGSamplingRate];
extern struct ACC_BUFFER ACCBuffer[2][SaveSecond*ACCSamplingRate];
extern struct ACC_PROFILE ADXL337;
extern struct PCG_PROFILE PCG;
extern unsigned char ucPDMBuffer[2][PDMBufferSize];
extern struct ExtSRAM_PROFILE ExtSRAM_PCG[2];
extern struct ExtSRAM_PROFILE ExtSRAM_ECG;
extern struct ExtSRAM_PROFILE ExtSRAM_ACC;
void Show_Error(void);
void Delay(unsigned int uiDelay);
void CheckRx1CommandRESET(char *cString);

#endif
