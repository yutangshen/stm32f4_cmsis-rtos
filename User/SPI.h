#ifndef __SPI_H
#define __SPI_H

#include "stm32f4xx_spi.h"


u8 SPI_TXRX(SPI_TypeDef* SPIx, u16 tx_data);
u32 SetupSPI1(void);
u32 SetupSPI2(void);
u32 SetupSPI3(void);
#if defined (STM32F427_437xx)
void SetupSPI4(void);
void SetupSPI6(void);
#endif

#endif

