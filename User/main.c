#include <string.h>
#include "config.h"
#include "main.h"
#include "usart.h"
#include "rtc.h"
#include "SRAM.h"
#include "sdcard.h"
#include "diskio.h"
#include "ff.h"
#include "SPI.h"
#include "ads129x.h"
#include "WaveFormat.h"

#define DEBUG_BAUDRATE 921600
#define BLUETOOTH_BAUDRATE 115200

struct OP_SYSTEM OP_STATUS;
struct USART_BUFFER Rx1Buffer[USART_NbrOfBuffer];
struct USART_BUFFER Rx2Buffer;
struct BLUETOOTH_PROFILE Bluetooth;
char HexString[]="0123456789ABCDEF";
char DataString[2*DEF_STRING_LENGTH];
char MessageString[2*DEF_STRING_LENGTH];
char BarString[]="========================================================";
char strFilenameBase[DEF_STRING_LENGTH];
float fCheck1,fCheck2,fCheck3;
RCC_ClocksTypeDef SystemClock;

//struct ECG_BUFFER ECGBuffer[2][SaveSecond*ECGSamplingRate];
//struct ACC_BUFFER ACCBuffer[2][SaveSecond*ACCSamplingRate];
//struct PCM_BUFFER PCG1Buffer[2][SaveSecond*PCMSamplingRate];
//struct PCM_BUFFER PCG2Buffer[2][SaveSecond*PCMSamplingRate];
unsigned char ucPDMBuffer[2][PDMBufferSize];

struct ACC_PROFILE ADXL337;
struct SAVE_FILE SaveACC;
struct ExtSRAM_PROFILE ExtSRAM_ACC;

struct SAVE_FILE SaveECG;
struct ExtSRAM_PROFILE ExtSRAM_ECG;

struct PCG_PROFILE PCG;
struct SAVE_FILE SavePCG[PCGChannel];
struct ExtSRAM_PROFILE ExtSRAM_PCG[PCGChannel];
struct WAVE_HEAD WaveWrite[PCGChannel];

struct SAVE_FILE SaveLOG;

short PDMtoPCMTable[256]={
0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4, 
1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5, 
2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6, 
3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7, 
4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8};    

void Show_Error(void)
{
	LedAllOff();
	OP_STATUS.bCaptureStart=false;
	OP_STATUS.bAlarm=true;
	for(;;) {

					
		if(OP_STATUS.bShutdown) {
				
			CloseAllFile();
			RTC_ShowNow(); printf("* SYSTEM: Shutdown ...");		
			OP_STATUS.bAlarm=false;

			SDCardOff();
			LedAllOn();
			PowerOff();
			for(;;);
		}
	}
}

/*******************************************************************************
* Function Name  : SetupGPIO
* Description    : Configures NVIC
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SetupGPIO(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOF,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOG,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOH,ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOI,ENABLE);
	
	// Card Reader RESET
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Speed = GPIO_Medium_Speed;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOG, &GPIO_InitStructure);
	
	// Regulator Enable: MCU(PI.02), SD:(PI.01);
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_1 | GPIO_Pin_2;
	GPIO_Init(GPIOI, &GPIO_InitStructure);
	
	// LED_R(Red)
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_10;
	GPIO_Init(GPIOF, &GPIO_InitStructure);

	// ACC_ST
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_4;
	GPIO_Init(GPIOH, &GPIO_InitStructure);
	GPIO_ResetBits(GPIOH, GPIO_Pin_4);

	// BT_RESET
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_Init(GPIOH, &GPIO_InitStructure);
		BluetoothOff();

	// BT_KEY
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
	GPIO_Init(GPIOH, &GPIO_InitStructure);

	// BT_STATUS
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
	GPIO_Init(GPIOH, &GPIO_InitStructure);
	
	// MCO
	GPIO_PinAFConfig(GPIOA, GPIO_PinSource8, GPIO_AF_MCO);
  	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  	GPIO_InitStructure.GPIO_Speed = GPIO_Medium_Speed;
  	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_8;
  	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  	GPIO_Init(GPIOA, &GPIO_InitStructure);	
	
	// Configure ADC1 Channel6 pin as analog input ******************************
	GPIO_InitStructure.GPIO_Speed = GPIO_Medium_Speed;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL ;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_0 | GPIO_Pin_1 | GPIO_Pin_3 | GPIO_Pin_4 ;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	// USB Detection
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Medium_Speed;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_Init(GPIOG, &GPIO_InitStructure);	
}

/*******************************************************************************
* Function Name  : SetupNVIC
* Description    : Configures NVIC
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SetupNVIC(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Configure the NVIC Preemption Priority Bits */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);

	/* Enable the SysTick Interrupt */
	NVIC_EnableIRQ(SysTick_IRQn);
	
	NVIC_InitStructure.NVIC_IRQChannel = SDIO_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	/* Enable the EXTI9_5 Stream IRQ Channel */
	NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 1;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
	
	/* Enable the DMA2_Stream0 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = DMA2_Stream0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);	

	/* Enable the USART1 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* Enable the USART2 Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

/*******************************************************************************
* Function Name  : Delay
* Description    : Delay for ms
* Input          : uiDelay (ms)
* Output         : None
* Return         : None
*******************************************************************************/
void Delay(unsigned int uiDelay)
{
	unsigned int i,j;
	if(OP_STATUS.bSysTickEnabled) {
		OP_STATUS.uiDelayCounter=0;
		for(;;) if(OP_STATUS.uiDelayCounter>=uiDelay) break;
	} else {
		for(i=0;i<uiDelay;i++) for(j=0;j<OP_STATUS.uiDelaySeed;j++);
	}
}

/*******************************************************************************
* Function Name  : SetupADC1_DMA2
* Description    : 
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
#define ADC1_BUFFERSIZE 5
uint16_t ADC1_ConvValues[ADC1_BUFFERSIZE];
#define ADC1_DMA_Stream DMA2_Stream4
void SetupADC1_DMA2(void)
{
	ADC_InitTypeDef       ADC_InitStructure;
	ADC_CommonInitTypeDef ADC_CommonInitStructure;
	DMA_InitTypeDef       DMA_InitStructure;

	DMA_DeInit(ADC1_DMA_Stream);
	
	/* DMA2 Stream0 channe0 configuration **************************************/
	DMA_InitStructure.DMA_Channel = DMA_Channel_0;  
	DMA_InitStructure.DMA_PeripheralBaseAddr = ((uint32_t)0x4001204C);
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)&ADC1_ConvValues;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
	DMA_InitStructure.DMA_BufferSize = ADC1_BUFFERSIZE; // Count of 16-bit words
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	// DMA_InitStructure.DMA_Mode = DMA_Mode_Normal;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_Full;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(ADC1_DMA_Stream, &DMA_InitStructure);
	/* DMA2_Stream0 enable */
	DMA_Cmd(ADC1_DMA_Stream, ENABLE);
	
	/* Enable DMA Transfer Complete interrupt */
	// DMA_ITConfig(ADC1_Stream, DMA_IT_TC, ENABLE);

	/* ADC Common Init **********************************************************/
	ADC_CommonInitStructure.ADC_Mode = ADC_Mode_Independent;
	ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div2;
	ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADC_CommonInit(&ADC_CommonInitStructure);	
   
	/* ADC1 Init ****************************************************************/

	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
	ADC_InitStructure.ADC_ScanConvMode = ENABLE; // 1 Channel
	ADC_InitStructure.ADC_ContinuousConvMode = ENABLE; // Conversions Triggered
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
	ADC_InitStructure.ADC_ExternalTrigConv = 0;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfConversion = ADC1_BUFFERSIZE;
	ADC_Init(ADC1,&ADC_InitStructure);


	ADC_TempSensorVrefintCmd(ENABLE);
	ADC_VBATCmd(ENABLE);
 
	/* ADC1 regular channel 10 configuration */
	// ADC_SampleTime_3Cycles
	// ADC_SampleTime_15Cycles  
	// ADC_SampleTime_28Cycles 
	// ADC_SampleTime_56Cycles   
	// ADC_SampleTime_84Cycles   
	// ADC_SampleTime_112Cycles  
	// ADC_SampleTime_144Cycles  
	// ADC_SampleTime_480Cycles  
 	ADC_RegularChannelConfig(ADC1, ADC_Channel_Vrefint, 1, ADC_SampleTime_480Cycles);
 	ADC_RegularChannelConfig(ADC1, ADC_VLIBAT, 2, ADC_SampleTime_480Cycles);
	ADC_RegularChannelConfig(ADC1, ADC_ACC_X, 3, ADC_SampleTime_480Cycles);
	ADC_RegularChannelConfig(ADC1, ADC_ACC_Y, 4, ADC_SampleTime_480Cycles);
	ADC_RegularChannelConfig(ADC1, ADC_ACC_Z, 5, ADC_SampleTime_480Cycles);
 
	/* Enable ADC1 */
	ADC_Cmd(ADC1, ENABLE);
 
	/* Enable DMA request after last transfer (Single-ADC mode) */
	ADC_DMARequestAfterLastTransferCmd(ADC1, ENABLE);


	/* Enable DMA request after last transfer (Multi-ADC mode)  */
	//ADC_MultiModeDMARequestAfterLastTransferCmd(ENABLE);
 
	/* Enable ADC1 DMA */
	ADC_DMACmd(ADC1, ENABLE);

	/* Enable ADC1 */
	ADC_Cmd(ADC1, ENABLE);

    ADC_SoftwareStartConv(ADC1);
}

/*******************************************************************************
* Function Name  : 
* Description    : 
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
#define SystemReferenceV (float)1.21
float GetVoltage(float fValue)
{
	return SystemReferenceV*fValue/OP_STATUS.fADCReference1V2;
}

/*******************************************************************************
* Function Name  : 
* Description    : 
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
#define TIMES_LiBAT (float)((910.0+390.0+390.0)/390.0)
float GetBatteryVoltage(void)
{
	return TIMES_LiBAT*GetVoltage(OP_STATUS.fADCLiBatteryV);
}

/*******************************************************************************
* Function Name  : FATFS_Init
* Description    : FAT File System Initialization
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
FATFS *pFS;
FATFS fs,fstest;                        // Work area (file system object) for logical drive
DWORD fre_clust;
DWORD fre_sect, tot_sect;
FRESULT res;
void FATFS_Init(void)
{ 
	SD_Error Status;
	char LabelString[16];

	RTC_ShowNow(); printf("* SD Card & FatFS Init ...");
	
	RTC_ShowNow(); printf("  Disk Initialization: %u", (unsigned int)(Status = (SD_Error)disk_initialize(0)));
    if (Status != RES_OK) {
        RTC_ShowNow();  printf("SD Card Initial(): FAIL (%d)",Status);        
        Show_Error();
    }	
	
	RTC_ShowNow(); printf("  Mount Disk: %d", res = f_mount(0,&fs));
    if (res != FR_OK) {
        RTC_ShowNow(); printf("  f_mount(): FAIL (%d)",res);        
        Show_Error();
    }	

	res = f_getlabel("", LabelString, 0);
	if (res != FR_OK) {
        RTC_ShowNow(); printf("  f_getlabel(): FAIL (%d)",res);        
        Show_Error();
    }
	RTC_ShowNow(); printf("  Volume Label: %s",LabelString);
	
	
	res=f_getfree("0:",&fre_clust, &pFS);
    if (res != FR_OK) {
        RTC_ShowNow(); printf("  f_getfree(): FAIL (%d)",res);        
        Show_Error();
    }

    // Get total sectors and free sectors
    fre_sect = fre_clust * pFS->csize;
    tot_sect = (pFS->n_fatent - 2) * pFS->csize;

    // Print free space in unit of KB (assuming 512 bytes/sector) 
    RTC_ShowNow(); printf("  SD Card (FATFS): %lu KB Total Space.",tot_sect / 2);
    RTC_ShowNow(); printf("  SD Card (FATFS): %lu KB Available.", fre_sect / 2);
}

/*******************************************************************************
* Function Name  : FATFS_Init
* Description    : FAT File System Initialization
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
void FATFS_Test(void)
{
	u32 i;
	UINT bw;
	FIL fp;
	char Filename[]=".test";

	RTC_ShowNow(); printf("* SD & FATFS Read/Write Test ...");
    RTC_ShowNow(); printf("  Prepare Write Buffer ... ");
	for(i=0;i<=SRAM_MAX_ADDR;i+=2) SRAM_u16Write(i,(u16)(i&0xFFFF));
	printf("%d Bytes",SRAM_MAX_ADDR+1);
	
	
	// f_open()
    RTC_ShowNow(); printf("  Creating /%s",Filename);	
    RTC_ShowNow(); printf("  f_open(): ");
	res=f_open(&fp, Filename, FA_CREATE_ALWAYS | FA_WRITE);
    if (res != FR_OK) {
        printf("FAIL (%d)",res);        
        Show_Error();
    }
    printf("Pass");

	// f_write()
    RTC_ShowNow(); printf("  f_write(): ");
	OP_STATUS.uiDelayCounter=0;
	res=f_write(&fp,(u32 *)ExtSRAMBase,SRAM_MAX_ADDR,&bw);
	i=OP_STATUS.uiDelayCounter;
    if (res != FR_OK) {
        printf("FAIL (%d)",res);        
        Show_Error();
    }
    printf("Pass");   
	
	// f_close()
    RTC_ShowNow(); printf("  f_close(): ");
	res=f_close(&fp);
    if (res != FR_OK) {
        printf("FAIL (%d)",res);        
        Show_Error();
    }
    printf("Pass (%d)",i);

    RTC_ShowNow(); printf("  Write Speed: %0.1f kBytes/Sec",(float)((double)(SRAM_MAX_ADDR+1)/(double)i));
	
}

/*******************************************************************************
* Function Name  : SetupADS129x
* Description    : 
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
void SetupADS129x(void)
{
	RTC_ShowNow(); printf("* ADS129x ECG AFE Initial ...");
	// Configure PA8 in MCO mode

	RTC_ShowNow(); printf("  MCO1 Initial ... ");	
	RCC_MCO1Config(RCC_MCO1Source_HSE,RCC_MCO1Div_4);
	Delay(250);
	printf("%u Hz ",HSE_VALUE/4);
    
 	RTC_ShowNow(); printf("  SPI2 Initial ... %d Hz",SystemClock.PCLK1_Frequency/SetupSPI2());	   
		
	ADS129x_Init();
	Delay(500);
	ADS129x_Check_Chipset();
	ADS129x_Load_Shift();
	ADS129x_Load_Calibration();		
	ADS129x_Load_Default();
	// ADS129x_Check_Parameter();

    ADS129x_WriteCMD(FCMD_RDATAC);

		
	OP_STATUS.uiECGUniPacketSize=ADS129x.uiDataNumber*sizeof(int);
	OP_STATUS.uiECGDataSize=SaveSecond*ADS129x.uiDataSamplingRate;
    ExtSRAM_ECG.u32Start[0]=0;
    ExtSRAM_ECG.u32Size[0]=OP_STATUS.uiECGUniPacketSize*OP_STATUS.uiECGDataSize;
    ExtSRAM_ECG.u32Start[1]=ExtSRAM_ECG.u32Start[0]+ExtSRAM_ECG.u32Size[0];
    ExtSRAM_ECG.u32Size[1]=OP_STATUS.uiECGUniPacketSize*OP_STATUS.uiECGDataSize;
    ExtSRAM_ECG.u32TotalSize=ExtSRAM_ECG.u32Size[0]+ExtSRAM_ECG.u32Size[1];
    
	SaveECG.u32SaveBufferSize=OP_STATUS.uiECGDataSize*OP_STATUS.uiECGUniPacketSize;
    RTC_ShowNow(); printf("  ExtSRAM(ECG): Start 0x%0.8X, Size: %d",ExtSRAM_ECG.u32Start[0],ExtSRAM_ECG.u32TotalSize);
	// RTC_ShowNow(); printf("  ECG Buffer Size = 2 * %u",SaveECG.u32SaveBufferSize);
	OP_STATUS.bECGInited=true;
}

void SaveECGParameter(void)
{
	FRESULT	res;
	unsigned int i;
	
	SaveECG.u32SaveIndex=0;

#ifdef SAVE_ECG_DATA		
	sprintf(SaveECG.strFilename,"%s/%s-ECG.%0.3u",strFilenameBase,strFilenameBase,SaveECG.u32SaveIndex);
	sprintf(MessageString,"* Save %s Parameter ...", ADS129x.cChipName);
	SaveToLog(MessageString);
	sprintf(MessageString,"  Filename: %s ...",&SaveECG.strFilename[16]);
	SaveToLog(MessageString);
	
	sprintf(DataString,"%d\r\n", ADS129x.uiDataSamplingRate);
	sprintf(DataString,"%s%d\r\n", DataString, ADS129x.uiDataNumber);
	sprintf(DataString,"%s%d, %d, %d, %d\r\n", DataString, ECGResp, ECGLead1, ECGLead2, ECGLead3);
	sprintf(DataString,"%s%0.1f\r\n", DataString, ADS129x.fReferenceV);	
	sprintf(DataString,"%s%0.3f\r\n", DataString, ADS129x.fCalValue);
	for(i=0;i<ADS129x.uiDataNumber;i++) {
		if(i==0) 
			sprintf(DataString,"%s%d", DataString, ADS129x.iOffset[i]);			
		else if(i!=(ADS129x.uiDataNumber-1) )
			sprintf(DataString,"%s, %d", DataString, ADS129x.iOffset[i]);
		else 
			sprintf(DataString,"%s, %d\r\n", DataString, ADS129x.iOffset[i]);
	}
	

	for(i=0;i<ADS129x.uiDataNumber;i++) {
		if(i==0) 
			sprintf(DataString,"%s%d", DataString, ADS129x.iCalibration[i]);			
		else if(i!=(ADS129x.uiDataNumber-1) )
			sprintf(DataString,"%s, %d", DataString, ADS129x.iCalibration[i]);
		else 
			sprintf(DataString,"%s, %d\r\n", DataString, ADS129x.iCalibration[i]);
	}
	
	res=f_open(&SaveECG.fdst, SaveECG.strFilename, FA_CREATE_ALWAYS | FA_WRITE);
    if (res != FR_OK) {
		sprintf(MessageString,"* ERR: f_open(ECG-0) = %d",res);        
		SaveToLog(MessageString);    
        Show_Error();
    }

	res=f_write(&SaveECG.fdst,DataString,strlen(DataString),&SaveECG.u32BytesWrite);
    if (res != FR_OK) {
		sprintf(MessageString,"* ERR: f_write(ECG-0) = %d (%d Bytes)", res, SaveECG.u32BytesWrite);        
		SaveToLog(MessageString);
    
        Show_Error();
    }

	res=f_close(&SaveECG.fdst);
    if (res != FR_OK) {
		sprintf(MessageString,"* ERR: f_close(ECG-0) = %d",res);        
		SaveToLog(MessageString);
        Show_Error();
    }
	
	sprintf(MessageString,"  File Saved!! ...");
	SaveToLog(MessageString);

	SaveLOGData(DataString);	

	
	SaveECG.u32TotalWrite=0;
	SaveECG.u32CountWrite=0;
    OP_STATUS.bECGInited=true;
#endif
}

/*******************************************************************************
* Function Name  : SaveECG
* Description    : Main program.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SaveECGData(void)
{
	u32 uiWriteIndex;
	

	OP_STATUS.bSaveECG=false;
#ifdef SAVE_ECG_DATA	
	LedAccessOn();
	uiWriteIndex=(OP_STATUS.uiECGDataIndex+1)&0x1;	// Writing ECG Data
	
	if(SaveECG.u32SaveCount==0) {
		SaveECG.u32SaveIndex++;

		//
		if(OP_STATUS.uiSaveFileCheckBatteryIndex<SaveECG.u32SaveIndex) {
			sprintf(MessageString,"* Battery Volatage: %0.3f V",GetBatteryVoltage());
			SaveToLog(MessageString);
			sprintf(MessageString,"* System Volatage: %0.3f V",GetVoltage(4095.0));
			SaveToLog(MessageString);
			OP_STATUS.uiSaveFileCheckBatteryIndex=SaveECG.u32SaveIndex;
		}

		//
		sprintf(SaveECG.strFilename,"%s/%s-ECG.%0.3u",strFilenameBase,strFilenameBase,SaveECG.u32SaveIndex);
		res=f_open(&SaveECG.fdst,SaveECG.strFilename,FA_CREATE_ALWAYS| FA_WRITE);
		if(res!=FR_OK) { 
			sprintf(MessageString,"* ERR: f_open() = %d",res);
			SaveToLog(MessageString);
			Show_Error();
		}
		sprintf(MessageString,"  %s was Created ... ",&SaveECG.strFilename[16]);
		SaveToLog(MessageString);
		SaveECG.u32CountWrite=0;
	} 
	
	res = f_write(&SaveECG.fdst,(uint32_t *)(ExtSRAMBase+ExtSRAM_ECG.u32Start[uiWriteIndex]),ExtSRAM_ECG.u32Size[uiWriteIndex],&SaveECG.u32BytesWrite);

	if(res!=FR_OK) {
		sprintf(MessageString,"* ERR: f_write() = %d (%d Bytes)", res, SaveECG.u32BytesWrite);
		SaveToLog(MessageString);
		Show_Error();
	}
	SaveECG.u32CountWrite+=SaveECG.u32BytesWrite;
	SaveECG.u32TotalWrite+=SaveECG.u32BytesWrite;
	
	if(++SaveECG.u32SaveCount>=SaveLimit) {
		SaveECG.u32SaveCount=0;
		
		res = f_close(&SaveECG.fdst);
		if(res!=FR_OK) {
			sprintf(MessageString,"* ERR: f_close() = %d",res);
			SaveToLog(MessageString);			
			Show_Error();
		}
	}	

	if(uiWriteIndex==OP_STATUS.uiECGDataIndex) Show_Error();
	LedAccessOff();
#endif	
}

/*******************************************************************************
* Function Name  : SetupACC
* Description    : 
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
void SetupACC(void)
{
	if(OP_STATUS.bECGInited==false) return;
	RTC_ShowNow();
	printf("* ADXL337 3-Axis Accelerometer Initial ...");
	
	ADXL337.uiDataSamplingRate=ACCSamplingRate;
	ADXL337.uiDataSamplingFactor=OP_STATUS.uiAFESamplingRate/ADXL337.uiDataSamplingRate;
	RTC_ShowNow(); printf("  Accelerometer Sampling Rate: %d Hz",ADXL337.uiDataSamplingRate);
	RTC_ShowNow(); printf("  Accelerometer Sampling Factor: %d",ADXL337.uiDataSamplingFactor);
	ADXL337.uiDataNumber=3;
	RTC_ShowNow(); printf("  Accelerometer 3-Axis: X, Y, Z");
	ADXL337.fValue0G[0]=GetVoltage(4095)/(float)2.0;
	ADXL337.fValue0G[1]=ADXL337.fValue0G[0];
	ADXL337.fValue0G[2]=ADXL337.fValue0G[0];
	RTC_ShowNow(); printf("  ACC 0g Base: %5.3f, %5.3f, %5.3f", ADXL337.fValue0G[0], ADXL337.fValue0G[2], ADXL337.fValue0G[2]);
	
	ADXL337.fValuePerG[0]=GetVoltage(4095)/(float)10.0;
	ADXL337.fValuePerG[1]=ADXL337.fValuePerG[0];
	ADXL337.fValuePerG[2]=ADXL337.fValuePerG[0];
	RTC_ShowNow(); printf("  ACC 1g Offset: %5.3f, %5.3f, %5.3f", ADXL337.fValuePerG[0], ADXL337.fValuePerG[2], ADXL337.fValuePerG[2]);
	
	OP_STATUS.uiACCUniPacketSize=ADXL337.uiDataNumber*sizeof(unsigned short);
	OP_STATUS.uiACCDataSize=SaveSecond*ADXL337.uiDataSamplingRate;
	SaveACC.u32SaveBufferSize=OP_STATUS.uiACCDataSize*OP_STATUS.uiACCUniPacketSize;
	RTC_ShowNow(); printf("  ACC Buffer Size = 2 * %u",SaveACC.u32SaveBufferSize);
    
    ExtSRAM_ACC.u32Start[0]=ExtSRAM_ECG.u32Start[0]+ExtSRAM_ECG.u32TotalSize;
    ExtSRAM_ACC.u32Size[0]=OP_STATUS.uiACCUniPacketSize*OP_STATUS.uiACCDataSize;
    ExtSRAM_ACC.u32Start[1]=ExtSRAM_ACC.u32Start[0]+ExtSRAM_ACC.u32Size[0];
    ExtSRAM_ACC.u32Size[1]=OP_STATUS.uiACCUniPacketSize*OP_STATUS.uiACCDataSize;
    ExtSRAM_ACC.u32TotalSize=ExtSRAM_ACC.u32Size[0]+ExtSRAM_ACC.u32Size[1];    
    RTC_ShowNow(); printf("  ExtSRAM(ACC): Start 0x%0.8X, Size: %d",ExtSRAM_ACC.u32Start[0],ExtSRAM_ACC.u32TotalSize);
	
    OP_STATUS.bACCInited=true;
}


/*******************************************************************************
* Function Name  : SaveACCParameter
* Description    : 
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
void SaveACCParameter(void)
{
	FRESULT	res;
	
	SaveACC.u32SaveIndex=0;
	
#ifdef SAVE_ACC_DATA	
	sprintf(SaveACC.strFilename,"%s/%s-ACC.%0.3u",strFilenameBase,strFilenameBase,SaveACC.u32SaveIndex);
	sprintf(MessageString,"* Save Accelerometer Parameter ...");
	SaveToLog(MessageString);
	sprintf(MessageString,"  Filename: %s ...",&SaveACC.strFilename[16]);
	SaveToLog(MessageString);
	
	sprintf(DataString,"%d\r\n", ADXL337.uiDataSamplingRate);
	sprintf(DataString,"%s%d\r\n", DataString, ADXL337.uiDataNumber);
	sprintf(DataString,"%s1.2, %0.3f\r\n", DataString, OP_STATUS.fADCReference1V2);
	sprintf(DataString,"%s%5.3f, %5.3f, %5.3f\r\n",DataString, ADXL337.fValue0G[0], ADXL337.fValue0G[1], ADXL337.fValue0G[2]);
	sprintf(DataString,"%s%5.3f, %5.3f, %5.3f\r\n",DataString, ADXL337.fValuePerG[0], ADXL337.fValuePerG[1], ADXL337.fValuePerG[2]);
	
	res=f_open(&SaveACC.fdst, SaveACC.strFilename, FA_CREATE_ALWAYS | FA_WRITE);
    if (res != FR_OK) {
		sprintf(MessageString,"* ERR: f_open(ACC-0) = %d",res);        
		SaveToLog(MessageString);
        Show_Error();
    }

	res=f_write(&SaveACC.fdst,DataString,strlen(DataString),&SaveACC.u32BytesWrite);
    if (res != FR_OK) {
		sprintf(MessageString,"* ERR: f_write(ACC-0) = %d (%d Bytes)", res, SaveACC.u32BytesWrite);        
		SaveToLog(MessageString);
        Show_Error();
    }

	res=f_close(&SaveACC.fdst);
    if (res != FR_OK) {
		sprintf(MessageString,"* ERR: f_close(ACC-0) = %d",res);        
		SaveToLog(MessageString);
        Show_Error();
    }
		
	sprintf(MessageString,"  File Saved!! ...");
	SaveToLog(MessageString);
	
	SaveLOGData(DataString);

	SaveACC.u32TotalWrite=0;
	SaveACC.u32CountWrite=0;
#endif		
}

/*******************************************************************************
* Function Name  : SaveACCData
* Description    : 
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
void SaveACCData(void)
{
	u32 uiWriteIndex;
	
	OP_STATUS.bSaveACC=false;

#ifdef SAVE_ACC_DATA		
	LedAccessOn();
	uiWriteIndex=(OP_STATUS.uiACCDataIndex+1)&0x1;	// Writing ACC Data
	if(SaveACC.u32SaveCount==0) {
		SaveACC.u32SaveIndex++;
		
		//
		if(OP_STATUS.uiSaveFileCheckBatteryIndex<SaveACC.u32SaveIndex) {
			sprintf(MessageString,"* Battery Volatage: %0.3f V",GetBatteryVoltage());
			SaveToLog(MessageString);
			sprintf(MessageString,"* System Volatage: %0.3f V",GetVoltage(4095.0));
			SaveToLog(MessageString);
			OP_STATUS.uiSaveFileCheckBatteryIndex=SaveACC.u32SaveIndex;
		}

		//
		sprintf(SaveACC.strFilename,"%s/%s-ACC.%0.3u",strFilenameBase,strFilenameBase,SaveACC.u32SaveIndex);
		res=f_open(&SaveACC.fdst,SaveACC.strFilename,FA_CREATE_ALWAYS| FA_WRITE);
		if(res!=FR_OK) { 
			sprintf(MessageString,"* ERR: f_open(ACC) = %d",res);
			SaveToLog(MessageString);
			Show_Error();
		}
		sprintf(MessageString,"  %s was Created ... ",&SaveACC.strFilename[16]);
		SaveToLog(MessageString);
		SaveACC.u32CountWrite=0;
	} 
	
	res = f_write(&SaveACC.fdst,(uint32_t *)(ExtSRAMBase+ExtSRAM_ACC.u32Start[uiWriteIndex]),ExtSRAM_ACC.u32Size[uiWriteIndex],&SaveACC.u32BytesWrite);

	if(res!=FR_OK) {
		sprintf(MessageString,"* ERR: f_write(ACC) = %d (%d Bytes)",res,SaveACC.u32BytesWrite);
		SaveToLog(MessageString);
		Show_Error();
	}
	SaveACC.u32CountWrite+=SaveACC.u32BytesWrite;
	SaveACC.u32TotalWrite+=SaveACC.u32BytesWrite;
	
	if(++SaveACC.u32SaveCount>=SaveLimit) {
		SaveACC.u32SaveCount=0;
		
		res = f_close(&SaveACC.fdst);
		if(res!=FR_OK) {
			sprintf(MessageString,"* ERR: f_close(ACC) = %d",res);
			SaveToLog(MessageString);			
			Show_Error();
		}
	}	

	if(uiWriteIndex==OP_STATUS.uiACCDataIndex) Show_Error();
	LedAccessOff();
#endif
}

/*******************************************************************************
* Function Name  : SetupADS129x
* Description    : 
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
void LoadWaveHead(u8 PCGn)
{
	// -----------------------------------------------------------------------------------------
    RTC_ShowNow(); printf("  Loading Wave File Head (%d) ... ",PCGn+1);
	WaveWrite[PCGn].ChunkID[0]='R'; 
	WaveWrite[PCGn].ChunkID[1]='I'; 
	WaveWrite[PCGn].ChunkID[2]='F'; 
	WaveWrite[PCGn].ChunkID[3]='F'; 

	WaveWrite[PCGn].Format[0]='W'; 
	WaveWrite[PCGn].Format[1]='A'; 
	WaveWrite[PCGn].Format[2]='V'; 
	WaveWrite[PCGn].Format[3]='E';
	 
	WaveWrite[PCGn].SubChunk1ID[0]='f';
	WaveWrite[PCGn].SubChunk1ID[1]='m';
	WaveWrite[PCGn].SubChunk1ID[2]='t';
	WaveWrite[PCGn].SubChunk1ID[3]=' ';
	WaveWrite[PCGn].SubChunk1Size=16;
	
	WaveWrite[PCGn].SubChunk2ID[0]='d';
	WaveWrite[PCGn].SubChunk2ID[1]='a';
	WaveWrite[PCGn].SubChunk2ID[2]='t';
	WaveWrite[PCGn].SubChunk2ID[3]='a';
	
	WaveWrite[PCGn].AudioFormat=1;	
	WaveWrite[PCGn].NumChannels=1;	
	WaveWrite[PCGn].SampleRate=PCG.uiDataSamplingRate;
	WaveWrite[PCGn].BitPerSample=16; 		//
	WaveWrite[PCGn].ByteRate=(WaveWrite[PCGn].SampleRate*WaveWrite[PCGn].NumChannels*WaveWrite[PCGn].BitPerSample)/8;
	WaveWrite[PCGn].BolckAlign=(WaveWrite[PCGn].BitPerSample*WaveWrite[PCGn].NumChannels)/8;
	
	WaveWrite[PCGn].SubChunk2Size=0;
    printf("Finished");
}

/*******************************************************************************
* Function Name  : SavePCG
* Description    : Main program.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SavePCGData(void)
{
	u32 uiWriteIndex;
    u32 i;
	
    OP_STATUS.bSavePCG=false;
	
#ifdef SAVE_PCG_DATA
	LedAccessOn();
	
	uiWriteIndex=(OP_STATUS.uiPCGDataIndex+1)&0x1;	// Writing PCG Data
    
    for(i=0;i<PCGChannel;i++) {
        if(SavePCG[i].u32SaveCount==0) {
            SavePCG[i].u32SaveIndex++;
			
			//
			if(OP_STATUS.uiSaveFileCheckBatteryIndex<SavePCG[i].u32SaveIndex) {
				sprintf(MessageString,"* Battery Volatage: %0.3f V",GetBatteryVoltage());
				SaveToLog(MessageString);
				sprintf(MessageString,"* System Volatage: %0.3f V",GetVoltage(4095.0));
				SaveToLog(MessageString);
				OP_STATUS.uiSaveFileCheckBatteryIndex=SavePCG[i].u32SaveIndex;
			}
			
			//
            sprintf(SavePCG[i].strFilename,"%s/%s-PCG.%0.3u.CH%d.Wav",strFilenameBase,strFilenameBase,SavePCG[i].u32SaveIndex,i+1);
            res=f_open(&SavePCG[i].fdst,SavePCG[i].strFilename,FA_CREATE_ALWAYS| FA_WRITE);
            if(res!=FR_OK) { 
                sprintf(MessageString,"* ERR: f_open(PCG-%d) = %d",i+1,res);
				SaveToLog(MessageString);
                Show_Error();
            }
            sprintf(MessageString,"  %s was Created ... ",&SavePCG[i].strFilename[16]);
			SaveToLog(MessageString);
            res = f_write(&SavePCG[i].fdst,&WaveWrite[i],sizeof(struct WAVE_HEAD),&SavePCG[i].u32BytesWrite);
            SavePCG[i].u32CountWrite=0;          
        } 
	
        res = f_write(&SavePCG[i].fdst,(uint32_t *)(ExtSRAMBase+ExtSRAM_PCG[i].u32Start[uiWriteIndex]),ExtSRAM_PCG[i].u32Size[uiWriteIndex],&SavePCG[i].u32BytesWrite);

        if(res!=FR_OK) {
            sprintf(MessageString,"* ERR: f_write(PCG-%d) = %d (%d Bytes)",i+1,res,SavePCG[i].u32BytesWrite);
			SaveToLog(MessageString);
            Show_Error();
        }
        SavePCG[i].u32CountWrite+=SavePCG[i].u32BytesWrite;
        SavePCG[i].u32TotalWrite+=SavePCG[i].u32BytesWrite;
        
        if(++SavePCG[i].u32SaveCount>=SaveLimit) {
            SavePCG[i].u32SaveCount=0;
			
            WaveWrite[i].SubChunk2Size=SavePCG[i].u32CountWrite;
			WaveWrite[i].ChunkSize = 4 + ( 8 + WaveWrite[i].SubChunk1Size) + (8 + WaveWrite[i].SubChunk2Size);			
			
            res = f_lseek(&SavePCG[i].fdst,0);
			if(res!=FR_OK) {
                sprintf(MessageString,"* ERR: f_lseek(PCG-%d) = %d\r\n",i+1,res);
				SaveToLog(MessageString);
				Show_Error();				
            }
            
			res = f_write(&SavePCG[i].fdst, &WaveWrite[i], sizeof(struct WAVE_HEAD),&SavePCG[i].u32BytesWrite);
			if(res!=FR_OK) {
				RTC_ShowNow(); 
                sprintf(MessageString,"* ERR: f_write(PCG-%d) = %d \r\n",i+1,res);
				SaveToLog(MessageString);
				Show_Error();
			}
            
            res = f_close(&SavePCG[i].fdst);
            if(res!=FR_OK) {
                sprintf(MessageString,"* ERR: f_close(PCG-%d) = %d",i+1,res);
				SaveToLog(MessageString);				
                Show_Error();
            }
        }
    }
	if(uiWriteIndex==OP_STATUS.uiPCGDataIndex) Show_Error();
	LedAccessOff();
#endif
}


/*******************************************************************************
* Function Name  : SetupADS129x
* Description    : 
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
void SetupPCG(void)
{
	u32 i,j;

	if(OP_STATUS.bECGInited==false) return;
	RTC_ShowNow(); printf("* PCG Capture Initial ...");

    //
    for(i=0;i<PDMBufferSize;i++) {
        ucPDMBuffer[0][i]=0;
        ucPDMBuffer[1][i]=0;
    }

    //
	RTC_ShowNow(); printf("  SPI3 Initial ... %d Hz (Slave)",SystemClock.PCLK1_Frequency/SetupSPI3());		
	RTC_ShowNow(); printf("  SPI1 Initial ... %d Hz (Master)",SystemClock.PCLK2_Frequency/SetupSPI1());	

    PCG.uiDataSamplingRate=OP_STATUS.uiAFESamplingRate;
    PCG.uiDataNumber=1;
    
    // Setup External SRAM for PCG1
    ExtSRAM_PCG[0].u32Start[0]=ExtSRAM_ECG.u32Start[0]+ExtSRAM_ECG.u32TotalSize+ExtSRAM_ACC.u32TotalSize;
    ExtSRAM_PCG[0].u32Size[0]=SaveSecond*PCG.uiDataSamplingRate*sizeof(short);
    ExtSRAM_PCG[0].u32Start[1]=ExtSRAM_PCG[0].u32Start[0]+ExtSRAM_PCG[0].u32Size[0];
    ExtSRAM_PCG[0].u32Size[1]=ExtSRAM_PCG[0].u32Size[0];
    ExtSRAM_PCG[0].u32TotalSize=ExtSRAM_PCG[0].u32Size[0]+ExtSRAM_PCG[0].u32Size[1];
    
    // Setup External SRAM for PCG2
    ExtSRAM_PCG[1].u32Start[0]=ExtSRAM_PCG[0].u32Start[0]+ExtSRAM_PCG[0].u32TotalSize;
    ExtSRAM_PCG[1].u32Size[0]=SaveSecond*PCG.uiDataSamplingRate*sizeof(short);
    ExtSRAM_PCG[1].u32Start[1]=ExtSRAM_PCG[1].u32Start[0]+ExtSRAM_PCG[1].u32Size[0];
    ExtSRAM_PCG[1].u32Size[1]=ExtSRAM_PCG[1].u32Size[0];
    ExtSRAM_PCG[1].u32TotalSize=ExtSRAM_PCG[1].u32Size[0]+ExtSRAM_PCG[1].u32Size[1];

    RTC_ShowNow(); printf("  PCG Sampling Rate: %d Hz",PCG.uiDataSamplingRate);		
    
	OP_STATUS.uiPCGUniPacketSize=PCG.uiDataNumber*sizeof(short);
	OP_STATUS.uiPCGDataSize=SaveSecond*PCG.uiDataSamplingRate;
	SavePCG[0].u32SaveBufferSize=OP_STATUS.uiPCGDataSize*OP_STATUS.uiPCGUniPacketSize;
	SavePCG[1].u32SaveBufferSize=OP_STATUS.uiPCGDataSize*OP_STATUS.uiPCGUniPacketSize;
	RTC_ShowNow(); printf("  ExtSRAM(PCG[0]): Start 0x%0.8X, Size: %d",ExtSRAM_PCG[0].u32Start[0],ExtSRAM_PCG[0].u32TotalSize);
    LoadWaveHead(0);    
	RTC_ShowNow(); printf("  ExtSRAM(PCG[1]): Start 0x%0.8X, Size: %d",ExtSRAM_PCG[1].u32Start[0],ExtSRAM_PCG[1].u32TotalSize);
    LoadWaveHead(1);    

    SetupSPI3_DMA();
    SetupSPI1_DMA();
    
    j=0;
    for(i=0;i<128;i++) {
        Delay(1);
        if(ucPDMBuffer[0][i]==0) j++;
    }
    if(j==127) Show_Error();


    j=0;
    for(i=0;i<128;i++) {
        Delay(1);
        if(ucPDMBuffer[1][i]==0) j++;
    }
    if(j==127) Show_Error();
    OP_STATUS.bPCGInited=true;
}

/*******************************************************************************
* Function Name  : SetFilenameBase
* Description    : Get Filename Base using the Date & Time
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
#define SPI1_DMA_Channel DMA_Channel_3
#define SPI1_DMA_Stream DMA2_Stream2
void SetupSPI1_DMA(void)
{
    DMA_InitTypeDef       DMA_InitStructure;

	RTC_ShowNow(); printf("  Setup SPI1 with DMA2 Stream0 ... ");		

    DMA_Cmd(SPI1_DMA_Stream, DISABLE);
    
    DMA_InitStructure.DMA_BufferSize = PDMBufferSize;
    DMA_InitStructure.DMA_Channel = SPI1_DMA_Channel;
    DMA_InitStructure.DMA_Memory0BaseAddr =(uint32_t)ucPDMBuffer[0];
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&SPI1->DR;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular; // DMA_Mode_Circular; //DMA_Mode_Normal;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
    DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
    DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
    DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
    DMA_Init(SPI1_DMA_Stream, &DMA_InitStructure);

    DMA_Cmd(SPI1_DMA_Stream, ENABLE);    
    
    SPI_I2S_DMACmd(SPI1, SPI_I2S_DMAReq_Rx, ENABLE);
    
    printf("Enabled");
}

/*******************************************************************************
* Function Name  : SetFilenameBase
* Description    : Get Filename Base using the Date & Time
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
#define SPI3_DMA_Channel DMA_Channel_0
#define SPI3_DMA_Stream DMA1_Stream2
void SetupSPI3_DMA(void)
{
    DMA_InitTypeDef       DMA_InitStructure;

	RTC_ShowNow(); printf("  Setup SPI3 with DMA1 Stream0 ... ");		

    DMA_Cmd(SPI3_DMA_Stream, DISABLE);
    
    DMA_InitStructure.DMA_BufferSize = PDMBufferSize;
    DMA_InitStructure.DMA_Channel = SPI3_DMA_Channel;
    DMA_InitStructure.DMA_Memory0BaseAddr =(uint32_t)ucPDMBuffer[1];
    DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)&SPI3->DR;
    DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
    DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
    DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
    DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_Byte;
    DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_Byte;
    DMA_InitStructure.DMA_Mode = DMA_Mode_Circular; // DMA_Mode_Circular; //DMA_Mode_Normal;
    DMA_InitStructure.DMA_Priority = DMA_Priority_High;
    DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
    DMA_InitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
    DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
    DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
    DMA_Init(SPI3_DMA_Stream, &DMA_InitStructure);

    DMA_Cmd(SPI3_DMA_Stream, ENABLE);    
    
    SPI_I2S_DMACmd(SPI3, SPI_I2S_DMAReq_Rx, ENABLE);
    
    printf("Enabled");
}

/*******************************************************************************
* Function Name  : SetFilenameBase
* Description    : Get Filename Base using the Date & Time
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
void SetFilenameBase(void) 
{	
    RTC_GetDate(RTC_Format_BIN, &RTC_DateStructure);
	RTC_GetTime(RTC_Format_BIN, &RTC_TimeStructure);
    sprintf(strFilenameBase,"20%0.2d%0.2d%0.2d_%0.2d%0.2d%0.2d", RTC_DateStructure.RTC_Year, RTC_DateStructure.RTC_Month, RTC_DateStructure.RTC_Date,RTC_TimeStructure.RTC_Hours, RTC_TimeStructure.RTC_Minutes, RTC_TimeStructure.RTC_Seconds);
}

/*******************************************************************************
* Function Name  : CloseAllFile
* Description    : FAT File System Initialization
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
void CloseAllFile(void)
{
    u32 i;
	if(SaveECG.fdst.fs) {			
		res = f_close(&SaveECG.fdst);
		if(res!=FR_OK) {
			RTC_ShowNow(); printf("* ERR: f_close(ECG)=%d",res);	
		}
	}
            
	if(SaveACC.fdst.fs) {			
		res = f_close(&SaveACC.fdst);
		if(res!=FR_OK) {
			RTC_ShowNow(); printf("* ERR: f_close(ACC)=%d",res);	
		}
	}	

    for(i=0;i<PCGChannel;i++) {
        if(SavePCG[i].fdst.fs) {
            WaveWrite[i].SubChunk2Size=SavePCG[i].u32CountWrite;
            WaveWrite[i].ChunkSize = 4 + ( 8 + WaveWrite[i].SubChunk1Size) + (8 + WaveWrite[i].SubChunk2Size);			
	
            res = f_lseek(&SavePCG[i].fdst,0);
            if(res!=FR_OK) {
                RTC_ShowNow(); printf("f_lseek(WAVE-%d): Head %d\r\n",i+1,res);
            }
            
            res = f_write(&SavePCG[i].fdst, &WaveWrite[i], sizeof(struct WAVE_HEAD),&SavePCG[i].u32BytesWrite);
            if(res!=FR_OK) {
                RTC_ShowNow(); 
                printf("\r\nf_write(WAVE-%d): Head %d \r\n",i+1,res);
                Show_Error();
            }
                    
            res = f_close(&SavePCG[i].fdst);
            if(res!=FR_OK) {
                RTC_ShowNow(); printf("* ERR: f_close(WAVE-%dC)=%d",i+1,res);	
            }
        }
    }

	if(SaveLOG.fdst.fs) {			
		res = f_close(&SaveLOG.fdst);
		if(res!=FR_OK) {
			RTC_ShowNow(); printf("* ERR: f_close(LOG)=%d",res);	
		}
	}	
}


/*******************************************************************************
* Function Name  : FATFS_Init
* Description    : FAT File System Initialization
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
char LogString[192];
void SaveToLog(char *cString)
{
	RTC_GetDateTimeString();
	sprintf(LogString,"%s %s",TimeString,cString);
	printf("\r\n%s",LogString);
	
	sprintf(LogString,"%s\r\n",LogString);
	res = f_write(&SaveLOG.fdst,LogString,strlen(LogString),&SaveLOG.u32BytesWrite);

	if(res!=FR_OK) {
		RTC_ShowNow(); printf("* ERR: f_write(LOG)=%d (%d Bytes)",res,SaveLOG.u32BytesWrite);
		Show_Error();
	}	
}

/*******************************************************************************
* Function Name  : FATFS_Init
* Description    : FAT File System Initialization
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/

void SaveLOGData(char *cString)
{
	sprintf(LogString,"%s\r\n",BarString);
	sprintf(LogString,"%s%s",LogString,cString);
	sprintf(LogString,"%s%s\r\n",LogString,BarString);
	
	res = f_write(&SaveLOG.fdst,LogString,strlen(LogString),&SaveLOG.u32BytesWrite);

	if(res!=FR_OK) {
		RTC_ShowNow(); printf("* ERR: f_write(LOG-Data) = %d (%d, %d Bytes)", res, strlen(LogString), SaveLOG.u32BytesWrite);
		Show_Error();
	}	
}

void SystemShutdown(void)
{
	RTC_ShowNow(); printf("* SYSTEM: Shutdown ...");		
	
	SDCardOff();
	LedAllOn();
	PowerOff();	
	for(;;);
}

u16 CheckTimeString(char *tString)
{
	u32 i;
	u16 u16Check;
	
	u16Check=0;
	for(i=0;i<strlen(tString);i++) if(tString[i]==',') u16Check++;
	if(u16Check!=6) return 1;
	return 0;
}

/*******************************************************************************
* Function Name  : CheckRx1Command
* Description    : 
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
void CheckRx1CommandRESET(char *cString)
{	
	if( (cString[0]=='R') &&
		(cString[1]=='E') &&
		(cString[2]=='S') &&
		(cString[3]=='E') &&
		(cString[4]=='T') ) {
		
		
		if(strlen(cString)!=5) {
			RTC_ShowNow(); printf("RX: RESET Command Length Error");
			return;
		}
		RTC_ShowNow(); printf("RX: %s",cString);
		NVIC_SystemReset();
	}
}


void CheckRx1Command(char *cString)
{
	u32 i;
	u16 u16Check;
	u32 u32Year, u32Month, u32Date, u32DayOfWeek;
	u32 u32Hour, u32Minute, u32Second;
	
	RTC_ShowNow(); printf("RX: %s",cString);
	
	if( (cString[0]=='T') &&
		(cString[1]=='E') &&
		(cString[2]=='S') &&
		(cString[3]=='T') &&
		(cString[4]=='=') ) {
			
		if(strlen(cString)!=6) {
			RTC_ShowNow(); printf("RX: TEST Command Length Error");
			return;
		}
		
		for(i=0;i<8;i++) if(cString[5]==HexString[i]) break;
		if(i==8) {
			RTC_ShowNow(); printf("RX: TEST Command Value Error");
			return;
		}
		
		switch(cString[5]) {
			case '0':
				OP_STATUS.ucTestType=TestTypeNon;
				break;
			case '1':
				OP_STATUS.ucTestType=TestTypeECG;
				break;
			case '2':
				OP_STATUS.ucTestType=TestTypeACC;
				break;
			case '3':
				OP_STATUS.ucTestType=TestTypePCG;
				break;
			case '4':
				OP_STATUS.ucTestType=TestTypeBLUETOOTH;
				break;
			case '5':
				OP_STATUS.ucTestType=TestTypeSetBLUETOOTH;
				break;
			case '6':
				OP_STATUS.ucTestType=TestTypeLED;
				break;
			case '7':
				OP_STATUS.ucTestType=TestTypeFileSystem;
				break;
		}
			
	} else if( (cString[0]=='T') &&
			   (cString[1]=='I') &&
		       (cString[2]=='M') &&
		       (cString[3]=='E') &&
			   (cString[4]=='=') ) {
		
		u16Check=CheckTimeString(&cString[5]);
		if(u16Check==0) {
			sscanf(&cString[5],"%d,%d,%d,%d,%d,%d,%d", &u32Year, &u32Month, &u32Date, &u32DayOfWeek, &u32Hour, &u32Minute, &u32Second); 
			// RTC_ShowNow(); printf("%d, %d, %d, %d , %d, %d, %d", u32Year, u32Month, u32Day, u32DayOfWeek, u32Hour, u32Minute, u32Second);
			
			//
			RTC_SetDateStructure.RTC_Year = (uint8_t) (u32Year);
			RTC_SetDateStructure.RTC_Month = (uint8_t) u32Month;
			RTC_SetDateStructure.RTC_Date = (uint8_t) u32Date;
			RTC_SetDateStructure.RTC_WeekDay = (uint8_t) u32DayOfWeek;	
			// Configure the RTC date register */
			if(RTC_SetDate(RTC_Format_BIN, &RTC_SetDateStructure) == ERROR) {
				printf("\r\n    >> RTC Set Date failed. !!");
				return;
			} 
			
			//
			RTC_SetTimeStructure.RTC_Hours = (uint8_t) u32Hour;
			RTC_SetTimeStructure.RTC_Minutes = (uint8_t) u32Minute;
			RTC_SetTimeStructure.RTC_Seconds = (uint8_t) u32Second;	
			if(RTC_SetTime(RTC_Format_BIN, &RTC_SetTimeStructure) == ERROR) {
				printf("\r\n    >> RTC Set Time failed.\r\n");
				return;
			}
			
			RTC_WriteBackupRegister(RTC_BKP_DR0, STD_BKP_DR0);			
			//
			// RTC_SetDateTime();

			// Allow access to RTC
			PWR_BackupAccessCmd(ENABLE);

			// Wait for RTC APB registers synchronisation
			RTC_WaitForSynchro();

			// Clear the RTC Alarm Flag
			RTC_ClearFlag(RTC_FLAG_ALRAF);

			// Clear the EXTI Line 17 Pending bit (Connected internally to RTC Alarm)
			EXTI_ClearITPendingBit(EXTI_Line17);
			
			//
			RTC_ShowNow(); printf("RX: TIME Setting Command Finished");			
			
	    } else if(u16Check==1) {
			RTC_ShowNow(); printf("RX: Command TIME Fail = Time String Fail");
		} else {
			RTC_ShowNow(); printf("RX: Command TIME Fail = Time String Fail");
		}
		
	} else {
		RTC_ShowNow(); printf("RX: Command Error");
		return;
	}
}


void CheckRx1(void)
{
	u32 i;
	for(i=0;i<2;i++) {
		if(Rx1Buffer[i].bCheck) {
			Rx1Buffer[0].bCheck=false;
			Rx1Buffer[1].bCheck=false;
			CheckRx1Command(Rx1Buffer[i].cCMDString);
		}		
	}	
}

/*******************************************************************************
* Function Name  : CheckRx2Command
* Description    : 
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
bool CheckRx2Command(char* cCMDString, char *cString)
{
	Rx2Buffer.usRxIndex=0;
	SendString2(cCMDString);
	Delay(333);

	if(Rx2Buffer.usRxIndex==strlen(cString)) {
		Rx2Buffer.cCMDString[Rx2Buffer.usRxIndex]='\0';
		printf("%s",Rx2Buffer.cCMDString);
		return true;
	}
	return false;
}

/*******************************************************************************
* Function Name  : WaitRx2Command
* Description    : 
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
bool WaitRx2Command(char* cCMDString)
{
	// u32 i;

	Rx2Buffer.usRxIndex=0;
	SendString2(cCMDString);
	Delay(250);

	if(Rx2Buffer.usRxIndex>0) {
		Rx2Buffer.cCMDString[Rx2Buffer.usRxIndex]='\0';
		/*
		if( (Rx2Buffer.cCMDString[0]=='O') &&
			(Rx2Buffer.cCMDString[1]=='K') &&
		    (Rx2Buffer.cCMDString[2]=='+') &&
		    (Rx2Buffer.cCMDString[3]=='G') &&
			(Rx2Buffer.cCMDString[4]=='e') &&
			(Rx2Buffer.cCMDString[5]=='t') &&
			(Rx2Buffer.cCMDString[6]==':') ) {
		*/		      
			return true;
		// } 
	}
	return false;
}

/*******************************************************************************
* Function Name  : SetupBluetooth
* Description    : 
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
void SetupBluetooth(void)
{
	//
	RTC_ShowNow(); printf("* Bluetooth Initial ... ");
	for(;;) if(CheckRx2Command("AT","OK")) break;

	//
	RTC_ShowNow(); printf("  Set Bluetooth Notify ... ");
	if(WaitRx2Command("AT+NOTI1")) {
		printf("%s",&Rx2Buffer.cCMDString[0]);
	} else printf("Fail");	
	
	if(!GetBluetoothHostname()) {
		RTC_ShowNow(); printf("* Get Bluetooth SPP/BLE Hostname & Address: Fail");
	}
}

/*******************************************************************************
* Function Name  : SetupBluetooth
* Description    : 
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
bool SetBluetoothHostname(void)
{
	char strCommand[32];
	u16 i;
	
	//
	RTC_ShowNow(); printf("* Set Bluetooth Hostname ... ");
	RTC_ShowNow(); printf("  Check AT Command ... ");	
	for(;;) if(CheckRx2Command("AT","OK")) break;
	
	//
	if(strlen(Bluetooth.strAddressSPP)>0) {
		//
		RTC_ShowNow(); printf("  Set Bluetooth SPP Hostname ... ");
		sprintf(strCommand,"AT+NAMEmySPP%s",&Bluetooth.strAddressSPP[6]);
		if(WaitRx2Command(strCommand)) {
			RTC_ShowNow(); printf("  Set Bluetooth SPP Hostname: "); 
			printf("%s",&Rx2Buffer.cCMDString[0]);
		} else {
			printf("Fail");
			return false;
		}

		//
		RTC_ShowNow(); printf("  Set Bluetooth BLE Hostname ... ");
		sprintf(strCommand,"AT+NAMBmyBLE%s",&Bluetooth.strAddressBLE[6]);
		if(WaitRx2Command(strCommand)) {
			RTC_ShowNow(); printf("  Set Bluetooth BLE Hostname: "); 
			printf("%s",&Rx2Buffer.cCMDString[0]);
		} else {
			printf("Fail");
			return false;
		}

		//
		SendString2("AT+RESET");
		RTC_ShowNow(); printf("* Reset Bluetooth, Please Wait ");
		for(i=0;i<10;i++) {
			Delay(333);
			putchar('.');
		}
		
	} else {
		RTC_ShowNow(); printf("* Bluetooth Device Not Ready!!");
		return false;		
	}
	return true;
}

bool GetBluetoothHostname(void) 
{
	//
	RTC_ShowNow(); printf("* Get Bluetooth SPP/BLE Hostname & Address: ");
	RTC_ShowNow(); printf("  Bluetooth SPP Address: ");
	if(WaitRx2Command("AT+ADDE?")) {
		printf("%s",&Rx2Buffer.cCMDString[7]);
		sprintf(Bluetooth.strAddressSPP,"%s",&Rx2Buffer.cCMDString[7]);
	} else {
		printf("Fail");
		return false;
	}
	//
	RTC_ShowNow(); printf("  Bluetooth SPP Hostname: ");
	if(WaitRx2Command("AT+NAME?")) {
		printf("%s",&Rx2Buffer.cCMDString[7]);
		sprintf(Bluetooth.strHostnameSPP,"%s",&Rx2Buffer.cCMDString[7]);
	} else {
		printf("Fail");
		return false;
	}	

	//
	RTC_ShowNow(); printf("  Bluetooth BLE Address: ");
	if(WaitRx2Command("AT+ADDB?")) {
		printf("%s",&Rx2Buffer.cCMDString[7]);
		sprintf(Bluetooth.strAddressBLE,"%s",&Rx2Buffer.cCMDString[7]);
	} else {
		printf("Fail");
		return false;
	}
	
	//
	RTC_ShowNow(); printf("  Bluetooth BLE Hostname: ");
	if(WaitRx2Command("AT+NAMB?")) {
		printf("%s",&Rx2Buffer.cCMDString[7]);
		sprintf(Bluetooth.strHostnameBLE,"%s",&Rx2Buffer.cCMDString[7]);
	} else {
		printf("Fail");
		return false;
	}	
	
	return true;
}

void ShowBluetoothHostname(void)
{
	RTC_ShowNow(); printf("  Bluetooth SPP Address: ");
	if(strlen(Bluetooth.strAddressSPP)>0) 
		printf("%s",Bluetooth.strAddressSPP);
	else printf("Fail");

	RTC_ShowNow(); printf("  Bluetooth SPP Hostname: ");
	if(strlen(Bluetooth.strHostnameSPP)>0) 
		printf("%s",Bluetooth.strHostnameSPP);
	else printf("Fail");

	RTC_ShowNow(); printf("  Bluetooth BLE Address: ");
	if(strlen(Bluetooth.strAddressBLE)>0) 
		printf("%s",Bluetooth.strAddressBLE);
	else printf("Fail");

	RTC_ShowNow(); printf("  Bluetooth BLE Hostname: ");
	if(strlen(Bluetooth.strHostnameBLE)>0) 
		printf("%s",Bluetooth.strHostnameBLE);
	else printf("Fail");	
}


/*******************************************************************************
* Function Name  : FATFS_Init
* Description    : FAT File System Initialization
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
int main(void)
{
	uint32_t i;
	
	// ------------------------------------------------------------------------- 
	//  System Flag and Parameter Initial ... 
	// ------------------------------------------------------------------------- 	
	OP_STATUS.bSysTickEnabled=false;
	OP_STATUS.bSRAMInited=false;
	OP_STATUS.ucTestType=TestTypeNon;
	
	// ------------------------------------------------------------------------- 
	//  Setup GPIO ...
	// ------------------------------------------------------------------------- 	
	SetupGPIO();

	// ------------------------------------------------------------------------- 
	//  Initial LED Status ...	
	//  Initial Power/SDCard/CardReader Power ...
	// ------------------------------------------------------------------------- 	
	LedAllOff();	
	SDCardOff();
	PowerOff();
	CardReader_Disable();
	BluetoothOff();
	OP_STATUS.bShutdown=false;

	
	// ------------------------------------------------------------------------- 
	//  Setup System Core Clock ...
	// ------------------------------------------------------------------------- 	
	SystemCoreClockUpdate();

	// ------------------------------------------------------------------------- 
	//  Get System Delay Seed ...
	// ------------------------------------------------------------------------- 	
	RCC_GetClocksFreq(&SystemClock);
	OP_STATUS.uiDelaySeed=SystemClock.SYSCLK_Frequency/1000/4;

	// ------------------------------------------------------------------------- 
	//  SYSCFG Clock Enabled ...
	// ------------------------------------------------------------------------- 	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);	

	// ------------------------------------------------------------------------- 
	//  Setup NVIC ...
	// ------------------------------------------------------------------------- 	
	SetupNVIC();/* Interrupt Config */

  	SetupUSART1(DEBUG_BAUDRATE);
    Delay(150);
    printf("\r\n\r\n\r\n\r\n");
	for(i=0;i<10;i++) {
		Delay(75);
		putchar('.');
	}

	// ------------------------------------------------------------------------- 
	//  Power On: LED On, Card Reader Disable
	// ------------------------------------------------------------------------- 
	PowerOn();
	LedAllOn();
	CardReader_Disable();
	BluetoothOn();		
    OP_STATUS.bPCGInited=false;
    OP_STATUS.bACCInited=false;
    OP_STATUS.bECGInited=false;

	// ------------------------------------------------------------------------- 
	//  Show Debug USART Parameter 
	// ------------------------------------------------------------------------- 	
	printf("\r\n* USART1: %u, N, 8, 1",DEBUG_BAUDRATE);	
	SetupUSART2(BLUETOOTH_BAUDRATE);
	printf("\r\n* USART2: %u, N, 8, 1",BLUETOOTH_BAUDRATE);	
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_ADC1, ENABLE);	
	printf("\r\n* ADC1 Enabled");
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA1, ENABLE);
	printf("\r\n* DMA1 Enabled");
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);
	printf("\r\n* DMA2 Enabled");
	
	// ------------------------------------------------------------------------- 
	//  Setup ADC1 with DMA2 
	// ------------------------------------------------------------------------- 	
	printf("\r\n* ADC1 (with DMA2): Start ... ");
	SetupADC1_DMA2();
	printf("Started");
	
	// ------------------------------------------------------------------------- 
	//  Show Program Title 
	// ------------------------------------------------------------------------- 	
  	printf("\r\n\r\n---=| ITRI-South MSTC ECG Holter V3 (Version %s) |=---\r\n",VERSION); 

	// ------------------------------------------------------------------------- 
	//  Show System Clock
	// ------------------------------------------------------------------------- 	
	RCC_GetClocksFreq(&SystemClock);
	printf("\r\n* System Clock Initial ...");
	printf("\r\n  System Core Clock: %u Hz",SystemClock.SYSCLK_Frequency);
	printf("\r\n  HCLK: %u Hz",SystemClock.HCLK_Frequency);
	printf("\r\n  PCLK1: %u Hz",SystemClock.PCLK1_Frequency );
	printf("\r\n  PCLK2: %u Hz",SystemClock.PCLK2_Frequency);
	
	// ------------------------------------------------------------------------- 
	//  Setup SysTick Interrupt Function
	// -------------------------------------------------------------------------
	i=SystemCoreClock / SysTickFrequency;	
	if ((i - 1) > (uint32_t)SysTick_LOAD_RELOAD_Msk) {
		printf("\r\n* SysTick: Fail");			
	} else {
		SysTick->LOAD  = i - 1;                                  // set reload register
		NVIC_SetPriority (SysTick_IRQn, (1<<__NVIC_PRIO_BITS) - 1);  // set Priority for Systick Interrupt
		SysTick->VAL   = 0;                                          // Load the SysTick Counter Value
		SysTick->CTRL  = SysTick_CTRL_CLKSOURCE_Msk |
                         SysTick_CTRL_TICKINT_Msk   |
                         SysTick_CTRL_ENABLE_Msk;                    // Enable SysTick IRQ and SysTick Timer
		printf("\r\n* SysTick: Pass (%d Hz)", SysTickFrequency);
		OP_STATUS.bSysTickEnabled=true;
	}
	
	// ------------------------------------------------------------------------- 
	//  Setup Real-Time Clock Function
	// ------------------------------------------------------------------------- 	
	printf("\r\n* Backup STD_BKP_DR0 (0x%0.4X): 0x%0.4X", STD_BKP_DR0, RTC_ReadBackupRegister(RTC_BKP_DR0));
	printf("\r\n* Setup RTC ...");
	SetupRTC();
	
	//
	Delay(250);
	for(;;) {
		Delay(25);
		if(!BT_Check()) break;
	};
	// ------------------------------------------------------------------------- 
	//  SD Card Power On
	// ------------------------------------------------------------------------- 	
	RTC_ShowNow(); printf("* SD Card: ");
	SDCardOn();
	printf("POWER ON");

	// ------------------------------------------------------------------------- 
	//  LED Testing
	// ------------------------------------------------------------------------- 	
	RTC_ShowNow(); printf("* LED Testing ");	

	for(i=0;i<3;i++) {
		LedAllOff();
		Delay(300);
		putchar('.');
		LedAllOn();
		Delay(300);		
		putchar('.');
	}
    printf(" Finished");
	
	// ------------------------------------------------------------------------- 
	//  System Voltage Detection
	// ------------------------------------------------------------------------- 	
	RTC_ShowNow(); printf("* Reference Volatage 1.2V: ");
	Delay(1500);
	printf("%f",OP_STATUS.fADCReference1V2);
	RTC_ShowNow(); printf("* Battery Volatage: %0.3f V",GetBatteryVoltage());
	RTC_ShowNow(); printf("* System Volatage: %0.3f V",GetVoltage(4095.0));
	
	// ------------------------------------------------------------------------- 
	//  External SRAM Initial & Testing 
	// ------------------------------------------------------------------------- 
	RTC_ShowNow(); printf("* Ext. SRAM Initial ... ");
	SRAM_Init();
	printf("Finished");
	SRAM_Test(1024*1024);

	// ------------------------------------------------------------------------- 
	//  SD Card & FAT/FAT16/FAT32 File System Initial 
	// ------------------------------------------------------------------------- 
	FATFS_Init();

	// ------------------------------------------------------------------------- 
	//  Setup ADS129x  
	// ------------------------------------------------------------------------- 
	SetupADS129x();
	OP_STATUS.uiAFESamplingRate=ADS129x.uiSystemSamplingRate;
	OP_STATUS.uiLEDFlashSeed=OP_STATUS.uiAFESamplingRate>>1;

	
	// ------------------------------------------------------------------------- 
	//  Setup ACC  
	// ------------------------------------------------------------------------- 
	SetupACC();

	// ------------------------------------------------------------------------- 
	//  Setup PCG  
	// ------ ------------------------------------------------------------------- 
	SetupPCG();

	// ------------------------------------------------------------------------- 
	//  System Voltage Detection
	// ------------------------------------------------------------------------- 	
	RTC_ShowNow(); printf("* Reference Volatage 1.2V: %0.3f",OP_STATUS.fADCReference1V2);
	RTC_ShowNow(); printf("* Battery Volatage: %0.3f V",GetBatteryVoltage());
	RTC_ShowNow(); printf("* System Volatage: %0.3f V",GetVoltage(4095.0));
    

	// ------------------------------------------------------------------------- 
	//  Check Usage of External SRAM 
	// ------------------------------------------------------------------------- 
    OP_STATUS.uiExtSRAMTotalUsed=ExtSRAM_ECG.u32TotalSize+ExtSRAM_ACC.u32TotalSize+ExtSRAM_PCG[0].u32TotalSize+ExtSRAM_PCG[1].u32TotalSize;
    RTC_ShowNow(); printf("* External SRAM total used: %d (0x%0.8X) Bytes",OP_STATUS.uiExtSRAMTotalUsed,OP_STATUS.uiExtSRAMTotalUsed);
    RTC_ShowNow(); printf("* 1 file for %d Minutes",(SaveSecond*SaveLimit)/60);

	// ------------------------------------------------------------------------- 
	//  Bluetooth Initial 
	// ------------------------------------------------------------------------- 
	SetupBluetooth();
	
	// ------------------------------------------------------------------------- 
	//  Holter Setting was Finished 
	// ------------------------------------------------------------------------- 
	RTC_ShowNow(); printf("* HOLTER: Initial was Finised !! ...");
	RTC_ShowNow(); printf("* HOLTER: Standby !! ...");	
	LedAllOff();
    LedStatusOn();

    // Test Process
	OP_STATUS.ucTestType=TestTypeNon;
	Rx1Buffer[0].bCheck=false;
	Rx1Buffer[1].bCheck=false;
	
	while(1)
	{
		if(BT_Check()) {
			// 按鍵按下長度超過 100ms 才啟動
			for(i=0;i<ButtonDelay;i++) {
				Delay(1);
				if(!BT_Check()) break;
			} 
			if(i<ButtonDelay) continue; 	
			
			// 按鍵按下未放開, 開啟 Shutdown 計數
			for(;;) {
				if(OP_STATUS.bShutdown) SystemShutdown();
				if(OP_STATUS.uiCheckBatteryCounter==0) break; 
			}
			
			// 按鍵放開長度超過 100ms 才啟動
			for(i=0;i<ButtonDelay;i++) {
				Delay(1);
				if(BT_Check()) break;
			} 
			if(i<ButtonDelay) continue; 
			
			// 開始量測
			ADS129x_CS_EN();
			ADS129x_START_HI();
			
			//
			SetFilenameBase();
			
			//
			res= f_mkdir(strFilenameBase);
			if(res != FR_OK) { 
				RTC_ShowNow(); printf("* ERR: f_mkdir(%d)",res);
				Show_Error();
			}
			
			//
			sprintf(SaveLOG.strFilename,"%s/%s.LOG",strFilenameBase,strFilenameBase);
			res=f_open(&SaveLOG.fdst,SaveLOG.strFilename,FA_CREATE_ALWAYS| FA_WRITE);
			if(res!=FR_OK) { 
				RTC_ShowNow(); printf("* ERR: f_open(LOG) = %d",res);
				Show_Error();
			}	

			RTC_ShowNow(); printf("%s",BarString);
			sprintf(MessageString,"* Filename Base: %s",strFilenameBase);
			SaveToLog(MessageString);
			sprintf(MessageString,"* Directary Created: /%s",strFilenameBase);
			SaveToLog(MessageString);
			sprintf(MessageString,"  Logging File Created: %s ",&SaveLOG.strFilename[16]);
			SaveToLog(MessageString);
            sprintf(MessageString,"* Battery Volatage: %0.3f V",GetBatteryVoltage());
			SaveToLog(MessageString);
			sprintf(MessageString,"* System Volatage: %0.3f V",GetVoltage(4095.0));
			SaveToLog(MessageString);
			OP_STATUS.uiSaveFileCheckBatteryIndex=0;


			RTC_ShowNow(); printf("* HOLTER Testing Start");
			
            //
			OP_STATUS.uiAFEDRDYCount=0;
			
			//
            SaveECG.u32SaveIndex=0;
			SaveECGParameter();
			OP_STATUS.uiECGDataIndex=0;
			OP_STATUS.uiECGDataCount=0;	
            SaveECG.u32SaveCount=0;
            
            //
            SaveACC.u32SaveIndex=0;
            SaveACCParameter();
			OP_STATUS.uiACCDataIndex=0;
			OP_STATUS.uiACCDataCount=0;
            SaveACC.u32SaveCount=0;

            //
            SavePCG[0].u32SaveIndex=0;
            SavePCG[1].u32SaveIndex=0;
			OP_STATUS.uiPCGDataIndex=0;
			OP_STATUS.uiPCGDataCount=0;
            SavePCG[0].u32SaveCount=0;
            SavePCG[1].u32SaveCount=0;
            
            //
			OP_STATUS.bCaptureStart=true;

			RTC_ShowNow(); printf("* HOLTER: Capture Started");		
			for(;;) {
				if(OP_STATUS.bSaveECG) SaveECGData();
                if(OP_STATUS.bSaveACC) SaveACCData();
                if(OP_STATUS.bSavePCG) SavePCGData();
									
				if(OP_STATUS.bShutdown) {
					OP_STATUS.bCaptureStart=false;
					ADS129x_CS_DI();
					ADS129x_START_LO();
					if(OP_STATUS.bSaveECG) SaveECGData();
					if(OP_STATUS.bSaveACC) SaveACCData(); 
					if(OP_STATUS.bSavePCG) SavePCGData();
					break;
				}
			}
			
			sprintf(MessageString,"** HOLTER: Capture Stop ...");
			SaveToLog(MessageString);
			
			CloseAllFile();
			SystemShutdown();
		}
		
		CheckRx1();
		
		if(OP_STATUS.ucTestType) 
		{
			RTC_ShowNow(); printf("%s",BarString);
			switch(OP_STATUS.ucTestType&0x7) {
				case TestTypeECG:
					RTC_ShowNow(); printf("* ECG Test Start");		
					//
					fCheck1=OP_STATUS.fADCReference1V2;
					fCheck2=(float)1.18*((float)4095.0/(float)2.8);
					fCheck3=(float)1.24*((float)4095.0/(float)2.8);				
					RTC_ShowNow(); printf("* Reference Volatage Value (%0.0f, %0.0f): %0.3f ",fCheck2,fCheck3,fCheck1);
					if( (fCheck1>(fCheck3)) || (fCheck1<(fCheck2)) ) printf("Fail"); else printf("O.K.");
					
					//
					fCheck1=GetVoltage(4095.0);
					fCheck2=(float)(2.8-(2.8*0.01));
					fCheck3=(float)(2.8+(2.8*0.01));
					RTC_ShowNow(); printf("* System Volatage (%0.3f, %0.3f): %0.3f ",fCheck2,fCheck3,fCheck1);					
					if( (fCheck1>(fCheck3)) || (fCheck1<(fCheck2)) ) printf("Fail"); else printf("O.K.");
					
					//
					RTC_ShowNow(); printf("* Battery Volatage: %0.3f V",GetBatteryVoltage());
					
					//
					ADS129x_Check_Parameter();
					RTC_ShowNow(); printf("* ECG Test Stop");
					OP_STATUS.ucTestType=TestTypeNon;
					break;
				
				case TestTypeACC:
					RTC_ShowNow(); printf("* ACC Test Start");	
					for(;;) {
						RTC_ShowNow(); printf("%d, %d, %d",ADC1_ConvValues[AD1_ACC_X],ADC1_ConvValues[AD1_ACC_Y],ADC1_ConvValues[AD1_ACC_Z]);
						Delay(50);
						CheckRx1();
						if(OP_STATUS.ucTestType!=TestTypeACC) break;
					}
					RTC_ShowNow(); printf("* ACC Test Stop");
					break;

				case TestTypePCG:
					RTC_ShowNow(); printf("* PCG Test Start");							
					for(;;) {
						RTC_ShowNow(); printf("0x%0.2X, 0x%0.2X",ucPDMBuffer[0][0],ucPDMBuffer[1][0]);
						Delay(50);
						CheckRx1();
						if(OP_STATUS.ucTestType!=TestTypePCG) break;
					}
					RTC_ShowNow(); printf("* PCG Test Stop");					
					break;

				case TestTypeBLUETOOTH:
					RTC_ShowNow(); printf("* Bluetooth Test Start");							
					
					if(!GetBluetoothHostname()) 
					{
						RTC_ShowNow(); printf("  Bluetooth Test: Fail");
					}
					
					RTC_ShowNow(); printf("* Bluetooth Test Stop");
					OP_STATUS.ucTestType=TestTypeNon;					
					break;		
				
				case TestTypeSetBLUETOOTH:
					RTC_ShowNow(); printf("* Set Bluetooth Hostname Start");							

					if(SetBluetoothHostname()) SetupBluetooth();
					
					RTC_ShowNow(); printf("* Set Bluetooth Hostname Stop");
					OP_STATUS.ucTestType=TestTypeNon;					
					break;	

				case TestTypeLED:
					RTC_ShowNow(); printf("* Test LED Status Start ");							
					for(;;) {
						LedAllOff();
						Delay(300);
						CheckRx1();
						if(OP_STATUS.ucTestType!=TestTypeLED) break;
						LedAllOn();
						Delay(300);		
					}
					LedStatusOn();
					RTC_ShowNow(); printf("* Test LED Status Stop");				
					break;
				
				case TestTypeFileSystem:
					RTC_ShowNow(); printf("* Test SD/Fat File System Start");
					FATFS_Test();				
					OP_STATUS.ucTestType=TestTypeNon;					
					RTC_ShowNow(); printf("* Test SD/Fat File System Finished");		
					break;
					
			}
		}
		
		
		
	}
}
