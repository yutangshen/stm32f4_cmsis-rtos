#ifndef __MAIN_H
#define __MAIN_H

#include <stdbool.h>

void SetupGPIO(void);
void SetupNVIC(void);
void SetupADC1_DMA2(void);
float GetVoltage(float fValue);
float GetBatteryVoltage(void);
void FATFS_Init(void);
void FATFS_Test(void);
void SetupADS129x(void);
void SetupSPI1_DMA(void);
void SetupSPI3_DMA(void);
void SaveToLog(char *cString);
void SaveLOGData(char *cString);
void CloseAllFile(void);
void SetupBluetooth(void);
bool SetBluetoothHostname(void);
bool GetBluetoothHostname(void);
void ShowBluetoothHostname(void);

#endif
