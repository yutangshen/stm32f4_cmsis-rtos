/*----------------------------------------------------------------------------
 * CMSIS-RTOS 'main' function template
 *---------------------------------------------------------------------------*/

#define osObjectsPublic                     // define objects in main module
#include "osObjects.h"                      // RTOS object definitions
#include "drivers.h"
#include "config.h"

const char *strSysClockSource[]={
"Internal Crystal",	
"External Crystal",
"PLL Clock"
};

char testString[20];

struct OP_SYSTEM OP_STATUS;
struct SENSOR_AVERAGE SensorAverage;
struct UART_RX DEBUG_RX[2];

static int Init_Mutex (void);
static void Init_Timers (void);


/*******************************************************************************
* Function Name  : Mutex_Init
* Description    : FAT File System Initialization
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
osMutexId MutexID_STDIO;                                     // mutex id
osMutexDef (MutexSTDIO);                                       // mutex name definition
osMutexId MutexID_GPRS;                                     // mutex id
osMutexDef (MutexGPRS);                                       // mutex name definition
osMutexId MutexID_MODBUS_TX;                                     // mutex id
osMutexDef (MutexMODBUS_TX);                                       // mutex name definition

static int Init_Mutex (void) 
{
  MutexID_STDIO = osMutexCreate  (osMutex (MutexSTDIO));
  if (!MutexID_STDIO)  {
    ; // Mutex object not created, handle failure
  }

  MutexID_GPRS = osMutexCreate  (osMutex (MutexGPRS));
  MutexID_MODBUS_TX = osMutexCreate  (osMutex (MutexMODBUS_TX));
  
  return(0);
}

/*******************************************************************************
* Function Name  : Mutex_Init
* Description    : FAT File System Initialization
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
/*----- One-Shoot Timer Example -----*/
unsigned int uiShowCounter=0;
static void Timer_ShowTime (void const *arg);                  				// prototype for timer callback function
static osTimerId TimerID_ShowTime;                                           // timer id
static uint32_t  TimerExec_ShowTime;                                         // argument for the timer call back function
static osTimerDef (TimerDef_ShowTime, Timer_ShowTime);                    // define timers

// One-Shoot Timer Function
static void Timer_ShowTime (void const *arg) 
{
  // add user code here
	osMutexWait(MutexID_STDIO, NULL);	
	if(uiShowCounter==0) {
		printf("\r\n"); CLOCK_ShowNow(); 
	} else {
		printf("\r\n"); CLOCK_ShowNow(); 
	}
	printf(" => %0.2f %0.2f", SensorAverage.fInternalReferenceVoltage, GetSystemVoltage());
	osMutexRelease(MutexID_STDIO);
	uiShowCounter++;
	ADC_SoftwareStartConv(ADC1);
}

// One-Shoot Timer Function
unsigned int uiSensorAverageCounter=0;
static void Timer_ADCTrigger (void const *arg);                  				// prototype for timer callback function
static osTimerId TimerID_ADCTrigger;                                           // timer id
static uint32_t  TimerExec_ADCTrigger;                                         // argument for the timer call back function
static osTimerDef (TimerDef_ADCTrigger, Timer_ADCTrigger);                    // define timers
static void Timer_ADCTrigger (void const *arg) 
{
  // add user code here
	SensorAverage.fTemp[0]+=(float)ADC_ConvValues[ADC_IntRefV];
	if( (uiSensorAverageCounter&0x1F) == 0x1F ) {
		SensorAverage.fInternalReferenceVoltage=SensorAverage.fTemp[0]/(float)32.0;
		SensorAverage.fTemp[0]=0;
	}
	uiSensorAverageCounter++;
	ADC_SoftwareStartConv(ADC1);
}

// Example: Create and Start timers
static void Init_Timers (void) 
{
  osStatus status;                                              // function return status
 
	// OS Timer for ADC Trigger
  TimerExec_ADCTrigger = 1;
  TimerID_ADCTrigger = osTimerCreate (osTimer(TimerDef_ADCTrigger), osTimerPeriodic, &TimerExec_ADCTrigger);
  if (TimerID_ADCTrigger != NULL) {    // One-shot timer created
    // start timer with delay 100ms
    status = osTimerStart (TimerID_ADCTrigger, 30);            
    if (status != osOK) {
      // Timer could not be started
    }
  }

  // Create one-shoot timer
  TimerExec_ShowTime = 2;

  // id1 = osTimerCreate (osTimer(Timer1), osTimerOnce, &exec1);
  TimerID_ShowTime = osTimerCreate (osTimer(TimerDef_ShowTime), osTimerPeriodic, &TimerExec_ShowTime);
  if (TimerID_ShowTime != NULL) {    // One-shot timer created
    // start timer with delay 100ms
    status = osTimerStart (TimerID_ShowTime, 1000);            
    if (status != osOK) {
      // Timer could not be started
    }
  }
}


/*******************************************************************************
* Function Name  : FATFS_Init
* Description    : FAT File System Initialization
* Input          : 
* Output         : None
* Return         : None
*******************************************************************************/
void FATFS_Test(void)
{
	UINT bw;
	FIL fp;
	FRESULT res;
	char Filename[]=".test";
	char strTestFile[]="0123456789ABCDEF";

  ShowOSTime(); printf("FATFS: File System Access Testing ...");	

	// f_open()
  ShowOSTime(); printf("FATFS:  => Creating /%s",Filename);	
  ShowOSTime(); printf("FATFS:  => f_open(): ");
	res=f_open(&fp, Filename, FA_CREATE_ALWAYS | FA_WRITE);
  if (res != FR_OK) {
    printf("FAIL (%d)",res);        
    SD_FatFS_ShowFSError(res);
  }
  printf("Pass");

	// f_write()
  ShowOSTime(); printf("FATFS:  => f_write(): ");
	res=f_write(&fp,(u32 *)strTestFile,sizeof(strTestFile),&bw);
  if (res != FR_OK) {
    printf("FAIL (%d)",res);        
    SD_FatFS_ShowFSError(res);
  }
  printf("Pass");   
	
	// f_close()
  ShowOSTime(); printf("FATFS:  => f_close(): ");
	res=f_close(&fp);
  if (res != FR_OK) {
    printf("FAIL (%d)",res);        
    SD_FatFS_ShowFSError(res);
  }
  printf("Pass");	
}

void Thread_DebugStringProcess(void const *argument)
{
  unsigned char *ucString=(unsigned char *)argument;

  if( (ucString[1] == 'T') && \
      (ucString[2] == 'I') && \
      (ucString[3] == 'M') && \
      (ucString[4] == 'E') && \
      (ucString[5] == '=') ) {
        
    CLOCK_makeDateTimeString();
    osMutexWait(MutexID_STDIO, osWaitForever);
    ShowOSTime(); printf(" => Sys TIME: %s", DateTimeString);
    osMutexRelease(MutexID_STDIO);

    CLOCK_Calibration(&ucString[6]);

    CLOCK_makeDateTimeString();
    osMutexWait(MutexID_STDIO, osWaitForever);
    ShowOSTime(); printf(" => New TIME: %s", DateTimeString);
    osMutexRelease(MutexID_STDIO);
  }
  osSignalSet(ThreadID_EventProcess, EventSignal_DebugStringProcess);      
  osThreadYield();
}


void Thread_EventProcess(void const *argument)
{
  osStatus    status;                                        // function return status
  osEvent     Evt;  
  
  while(1)
  {
    // Evt=osSignalWait(0, osWaitForever);
    Evt=osSignalWait(0, osWaitForever);
    if(Evt.status == osEventSignal) {
      switch(Evt.value.signals & 0xFFFFFFF0) {
        // --------------------------------------------------------------------
        // Sensor Capture Event
        // --------------------------------------------------------------------
				/*
        case EventSignal_SystemProcess:
          switch(Evt.value.signals) {
            case EventSignal_SystemAlarmProcessStart:
              status = osTimerStop (TimerID_ADCAverage);     
              osMutexWait(MID_DebugUart_Mutex, osWaitForever);              
              ShowOSTime(); printf("RTX-Stopped: \"ADCAverage\"");        
              osMutexRelease(MID_DebugUart_Mutex);

              TID_Thread_LEDAlarm=osThreadCreate (osThread (Thread_LEDAlarm), NULL);
              osMutexWait(MID_DebugUart_Mutex, osWaitForever);              
              ShowOSTime(); printf("RTX-Created: \"LEDAlarm\" (%0.8X, %0.2X)", (u32)TID_Thread_LEDAlarm, OP_STATUS.ucAlarmCode);
              osMutexRelease(MID_DebugUart_Mutex);
              break;

            case EventSignal_SystemAlarmProcessStop:
              osThreadTerminate (TID_Thread_LEDAlarm);
              LED_Off(LED_Red);
              TID_Thread_LEDAlarm=NULL;
              osMutexWait(MID_DebugUart_Mutex, osWaitForever);              
              ShowOSTime(); printf("RTX-Terminal: \"LEDAlarm\" (%0.8X)", (u32)TID_Thread_LEDAlarm);
              ShowOSTime(); printf("========================================");         
              osMutexRelease(MID_DebugUart_Mutex);
              
              status = osTimerStart (TimerID_ADCAverage, CaptureTime); 
              osMutexWait(MID_DebugUart_Mutex, osWaitForever);              
              ShowOSTime(); printf("RTX-Started: \"ADCAverage\"");       
              osMutexRelease(MID_DebugUart_Mutex);
              break;
          }
          break;
        */
				
        // --------------------------------------------------------------------
        // Sensor Capture Event
        // --------------------------------------------------------------------
				/*
        case EventSignal_SensorCapture:
          switch(Evt.value.signals) {
            case EventSignal_SensorCaptureStart:
              TID_Thread_SensorCapture=osThreadCreate (osThread(Thread_SensorCapture), NULL);
              osMutexWait(MID_DebugUart_Mutex, osWaitForever);              
              ShowOSTime(); printf("RTX-Created: \"SensorCapture\" (%0.8X)", (u32)TID_Thread_SensorCapture);  
              osMutexRelease(MID_DebugUart_Mutex);
              break;

            case EventSignal_SensorCaptureStop:
              osThreadTerminate (TID_Thread_SensorCapture); 
              TID_Thread_SensorCapture=NULL;
              osMutexWait(MID_DebugUart_Mutex, osWaitForever);              
              ShowOSTime(); printf("RTX-Terminal: \"SensorCapture\" (%0.8X)", (u32)TID_Thread_SensorCapture);  
              ShowOSTime(); printf("========================================");         
              osMutexRelease(MID_DebugUart_Mutex);
              break;
          }
          break;
        */
          
        // --------------------------------------------------------------------
        // Debug Port Command Event
        // --------------------------------------------------------------------
        case EventSignal_DebugStringProcess:
          if( Evt.value.signals == EventSignal_DebugStringProcess ) {
            osThreadTerminate (ThreadID_DebugStringProcess); 
            ThreadID_DebugStringProcess=NULL;
            osMutexWait(MutexID_STDIO, osWaitForever);              
            ShowOSTime(); printf("RTX-Terminal: \"DebugStringProcess\" (%0.8X)", (u32)ThreadID_DebugStringProcess);            
            osMutexRelease(MutexID_STDIO);
          } else {
            ThreadID_DebugStringProcess = osThreadCreate (osThread (Thread_DebugStringProcess), DEBUG_RX[(Evt.value.signals&0xF)-1].ucString);
            osMutexWait(MutexID_STDIO, osWaitForever);              
            ShowOSTime(); printf("========================================");         
            ShowOSTime(); printf("RTX-Created: \"DebugStringProcess\" (%0.8X)", (u32)ThreadID_DebugStringProcess);         
            osMutexRelease(MutexID_STDIO);
          }
          break;
      }      
    }
    osThreadYield();
  }  
}


/*
 * main: initialize and start the system
 */
int main (void) {
	unsigned int i,n;
	
  osKernelInitialize ();                    // initialize CMSIS-RTOS

  // initialize peripherals here
	RCC_Setup();

	//  Configure the NVIC Preemption Priority Bits
	NVIC_Setup();
	
	//
	ADC_DMA_Setup();
	
	//
	DEBUG_UART_Setup(921600);
			
	// show system information
	printf("\r\n\n\n");

	// ----------------------------------------------------------------------
	//  Show CPU information
	// ----------------------------------------------------------------------
	for(i=0; i<usDeviceRevIDNumber; i++) if(  DBGMCU_GetDEVID()==DeviceID[i] ) break;
	ShowOSTime(); printf("CPU: %s", DeviceIDString[usDeviceValue=i]);
	for(i=0; i<usDeviceRevIDNumber; i++) if(  DBGMCU_GetREVID() == DeviceRevID[i] ) break;
	ShowOSTime(); printf("CPU: %s", DeviceRevIDString[usDeviceRevValue=i]);
	ShowOSTime(); printf("CPU: Internel Flash %d kBytes", CPU_GetFlashSize() );  
	ShowOSTime(); printf("CPU: Internal SRAM %d kBytes", usDeviceRAMSize[usDeviceValue]);  
	ShowOSTime(); printf("CPU: ID %0.8X %0.8X %0.8X", CPU_GetID(0), CPU_GetID(1), CPU_GetID(2));	
	

	// ----------------------------------------------------------------------
	//  Check CMSIS & Standard Peripheral Library Version
	// ----------------------------------------------------------------------
	ShowOSTime(); printf("CMSIS: Cortex-M%x Version %x.%x",__CORTEX_M,__CM4_CMSIS_VERSION>>16,__CM4_CMSIS_VERSION&0x0000FFFF);
	ShowOSTime(); printf("CMSIS: CMSIS-RTOS API version %d.%0.2d", osCMSIS>>16, osCMSIS&0xFF);
	ShowOSTime(); printf("CMSIS: Kernel %s", osKernelSystemId);	
	ShowOSTime(); printf("CMSIS: STM32F4xx Standard Peripheral Library V%X.%X.%X",(__STM32F4XX_STDPERIPH_VERSION>>24)&0xFF, \
																																								(__STM32F4XX_STDPERIPH_VERSION>>16)&0xFF, \
																																								(__STM32F4XX_STDPERIPH_VERSION>>8)&0xFF);	


	// ----------------------------------------------------------------------
	//  Show Firmware information
	// ----------------------------------------------------------------------
	ShowOSTime(); printf("FIRMWARE: Version %d.%0.2d", FIRMWARE_VERSION_MAIN, FIRMWARE_VERSION_SUB);
	ShowOSTime(); printf("FIRMWARE: Compile Date %s", FIRMWARE_DATE);
	ShowOSTime(); printf("FIRMWARE: Compile Time %s", FIRMWARE_TIME);	
	
	// ----------------------------------------------------------------------
	//  System Clock Initial ...
	// ----------------------------------------------------------------------
	i=RCC_GetSYSCLKSource();
	if(i==0x08) n=2;
	else if(i==0x04) n=1;
	else n=0;
	ShowOSTime(); printf("SYSTEM: Clock Source was %s",strSysClockSource[n]);
	if(RCC->PLLCFGR & RCC_PLLSource_HSE) {
		ShowOSTime(); printf("SYSTEM: PLL Source was External Clock %d Hz", HSE_VALUE);
	} else {
		ShowOSTime(); printf("SYSTEM: PLL Source was Internal Clock %d Hz", HSI_VALUE);
	}
	ShowOSTime(); printf("SYSTEM: Core Clock %d Hz", SystemClocks.SYSCLK_Frequency);
	ShowOSTime(); printf("SYSTEM: HCLK Clock %d Hz", SystemClocks.HCLK_Frequency);
	ShowOSTime(); printf("SYSTEM: PCLK1 Clock %d Hz", SystemClocks.PCLK1_Frequency);
	ShowOSTime(); printf("SYSTEM: PCLK2 Clock %d Hz", SystemClocks.PCLK2_Frequency);
	
	// ----------------------------------------------------------------------
	//  Real-Time Clock Initial ...
	// ----------------------------------------------------------------------
	CLOCK_Setup();
	ShowOSTime(); printf("CLOCK: Time Value 0x%0.8X", GPRS_GetNowValue());
	
	// ----------------------------------------------------------------------
	//  SD Card & FAT File System Initial ...
	// ----------------------------------------------------------------------
	if (SD_FatFS_Setup() == FR_OK) {
		ShowOSTime(); printf("FATFS: SD Card/FAT System was Initialized");
	} else {
		ShowOSTime(); printf("FATFS: SD Card/FAT was Fail");
	}
	
	// ----------------------------------------------------------------------
	//  FATFS Access Testing ...
	// ----------------------------------------------------------------------
	// FATFS_Test();
	
  // create 'thread' functions that start executing,
  // example: tid_name = osThreadCreate (osThread(name), NULL);
	LED_Setup();
	Init_Mutex();
	Init_Timers();

  ThreadID_EventProcess=osThreadCreate (osThread (Thread_EventProcess), NULL);
  ShowOSTime(); printf("RTX-Created: \"EventProcess\" (0x%0.8X)", (u32)ThreadID_EventProcess);
	
  osKernelStart ();                         // start thread execution 
  osMutexWait(MutexID_STDIO, osWaitForever);
	ShowOSTime(); printf("CMSIS-RTOS: Started");
	osMutexRelease(MutexID_STDIO);  
	
  while(1)
  {
    for(i=0;i<2;i++) {
      if(DEBUG_RX[i].bCheck) {
        DEBUG_RX[i].bCheck=false;
				osMutexWait(MutexID_STDIO, osWaitForever);
				ShowOSTime(); printf("DEBUG-RX: %s", DEBUG_RX[i].ucString);
        osSignalSet(ThreadID_EventProcess, EventSignal_DebugStringProcess|(i+1));
				osMutexRelease(MutexID_STDIO);  
      }
    }
    osThreadYield();
	}
}
