#ifndef __CONFIG_H
#define __CONFIG_H

#include <cmsis_os.h>    
#include <stdio.h>
#include <stdbool.h>

#define FIRMWARE_VERSION_MAIN 0
#define FIRMWARE_VERSION_SUB 1
#define FIRMWARE_DATE __DATE__
#define FIRMWARE_TIME __TIME__

struct OP_SYSTEM {
  unsigned char   ucDebugRxIndex;
  unsigned char   ucAlarmCode;
};

struct SENSOR_AVERAGE {
	float		fTemp[3];
	float		fInternalReferenceVoltage;
	float		fExternalReferenceVoltage;	
};


struct UART_RX {
	unsigned 	char 	ucIndex;
						bool	bCheck;
	unsigned 	char	ucString[32];
};

// global 'semaphores' ----------------------------------------------------------
/* 
Example:
osMutexId mid_sample_name;                          // Mutex id
osMutexDef (sample_name);                           // Mutex object
*/
extern osMutexId MutexID_STDIO;                     // mutex id
extern osMutexId MutexID_GPRS;                     	// mutex id
extern osMutexId MutexID_MODBUS_TX;                 // mutex id

extern struct OP_SYSTEM OP_STATUS;
extern struct SENSOR_AVERAGE SensorAverage;
extern struct UART_RX DEBUG_RX[];

extern char testString[];
extern void ShowOSTime(void);

#endif
